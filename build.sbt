name := "base"
organization := "ch.epfl.dias.cs422"

version := "2022.1.2"

scalaVersion := "2.13.1"

val calciteVersion = "1.26.0"

initialize := {
  val _ = initialize.value // run the previous initialization
  val required = VersionNumber("10")
  val current = VersionNumber(sys.props("java.specification.version"))
  assert(
    current.matchesSemVer(SemanticSelector(">=10")),
    s"Unsupported JDK: java.specification.version $current < $required"
  )
}

scalacOptions in (Compile, doc) ++= Seq(
  "-groups",
  "-skip-packages",
  "ch.epfl.dias.cs422.helpers.rel.late.volcano:ch.epfl.dias.cs422.rel:ch.epfl.dias.cs422.helpers.qo"
)

autoAPIMappings := true

// https://mvnrepository.com/artifact/org.apache.calcite/calcite-core
// Include Calcite Core
libraryDependencies += "org.apache.calcite" % "calcite-core" % calciteVersion
// Also include the tests.jar of Calcite Core as a dependency to our testing jar
libraryDependencies += "org.apache.calcite" % "calcite-core" % calciteVersion % Test classifier "tests"
// Calcite DDL Parser
// https://mvnrepository.com/artifact/org.apache.calcite/calcite-server
libraryDependencies += "org.apache.calcite" % "calcite-server" % calciteVersion
// https://mvnrepository.com/artifact/org.apache.calcite.avatica/avatica
libraryDependencies += "org.apache.calcite.avatica" % "avatica-server" % "1.17.0"

libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value

// https://mvnrepository.com/artifact/au.com.bytecode/opencsv
libraryDependencies += "au.com.bytecode" % "opencsv" % "2.4"

// slf4j
libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.25"
libraryDependencies += "org.slf4j" % "slf4j-log4j12" % "1.7.25"

// https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api
libraryDependencies += "org.junit.jupiter" % "junit-jupiter-api" % "5.3.1" % Test
libraryDependencies += "org.junit.jupiter" % "junit-jupiter-params" % "5.3.1" % Test

// junit tests (invoke with `sbt test`)
libraryDependencies += "com.novocode" % "junit-interface" % "0.11" % "test"

fork in Test := true

// disable tests during `sbt assembly`
test in assembly := {}

publishArtifact in (Test, packageBin) := true
publishArtifact in (Test, packageSrc) := true
publishArtifact in (Test, packageDoc) := true

resolvers += Resolver.jcenterRepo
testOptions += Tests.Argument(jupiterTestFramework, "-q")

libraryDependencies += "net.aichler" % "jupiter-interface" % JupiterKeys.jupiterVersion.value % Test

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "services", _ @_*) => MergeStrategy.first
  case PathList("META-INF", _ @_*)             => MergeStrategy.discard
}
