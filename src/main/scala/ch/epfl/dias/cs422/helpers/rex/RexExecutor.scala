/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.rex

import ch.epfl.dias.cs422.helpers.rel.RelOperator.{
  HomogeneousColumn,
  toHomogeneousColumnOfKnownType
}
import ch.epfl.dias.cs422.helpers.rex.RexExecutor.logGeneratedCode
import com.google.common.collect.ImmutableList
import org.apache.calcite.DataContext
import org.apache.calcite.adapter.enumerable.RexToLixTranslator
import org.apache.calcite.adapter.java.JavaTypeFactory
import org.apache.calcite.jdbc.JavaTypeFactoryImpl
import org.apache.calcite.linq4j.tree._
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rex._
import org.apache.calcite.runtime.{SqlFunctions, Utilities}
import org.apache.calcite.sql.validate.SqlConformanceEnum
import org.apache.calcite.util.{BuiltInMethod, Util}
import org.codehaus.commons.compiler.CompileException
import org.codehaus.janino.{ClassBodyEvaluator, Scanner}
import org.slf4j.LoggerFactory

import java.io._
import java.lang.reflect.{InvocationTargetException, Modifier, Type}
import java.util
import scala.jdk.CollectionConverters._
import scala.reflect.ClassTag

private[helpers] class RexExecutor extends RexExecutorImpl(new TupleContext) {
  private def compile(
      rexBuilder: RexBuilder,
      constExps: util.List[RexNode],
      getter: RexToLixTranslator.InputGetter,
      rowType: RelDataType
  ): String = {
    val programBuilder = new RexProgramBuilder(rowType, rexBuilder)
    for (node <- constExps.asScala) {
      programBuilder.addProject(node, "c" + programBuilder.getProjectList.size)
    }
    val javaTypeFactory = new JavaTypeFactoryImpl(
      rexBuilder.getTypeFactory.getTypeSystem
    )
    val blockBuilder = new BlockBuilder
    val root0_ = Expressions.parameter(classOf[Any], "root0")
    val root_ = DataContext.ROOT
    blockBuilder.add(
      Expressions.declare(
        Modifier.FINAL,
        root_,
        Expressions.convert_(root0_, classOf[DataContext])
      )
    )
    val conformance = SqlConformanceEnum.DEFAULT
    val program = programBuilder.getProgram
    val expressions = RexToLixTranslator.translateProjects(
      program,
      javaTypeFactory,
      conformance,
      blockBuilder,
      null,
      root_,
      getter,
      null
    )
    blockBuilder.add(
      Expressions.return_(
        null,
        Expressions.newArrayInit(classOf[Array[AnyRef]], expressions)
      )
    )
    val methodDecl = Expressions.methodDecl(
      Modifier.PUBLIC,
      classOf[Array[AnyRef]],
      BuiltInMethod.FUNCTION1_APPLY.method.getName,
      ImmutableList.of(root0_),
      blockBuilder.toBlock
    )
    logGeneratedCode(Expressions.toString(methodDecl))
  }
}

private[helpers] object RexExecutor {
  protected def logGeneratedCode(code: String): String = {
    val logger = LoggerFactory.getLogger(classOf[RexExecutor])
    if (logger.isDebugEnabled) {
      val buf = new ByteArrayOutputStream()
      Util.debugCode(new PrintStream(buf), code)
      logger.debug(buf.toString)
    }
    code
  }

  class DataContextInputGetter(
      rowType: RelDataType,
      typeFactory: JavaTypeFactory
  ) extends RexToLixTranslator.InputGetter {
    override def field(
        list: BlockBuilder,
        index: Int,
        storageType: Type
    ): Expression = {
      //      val expr = DataContext.ROOT.asInstanceOf[TupleContext].fieldAsExpression(index)
      //      if (expr!= null) return expr

      val method = classOf[TupleContext].getMethod("field", classOf[Int])
      val context =
        Expressions.convert_(DataContext.ROOT, classOf[TupleContext])
      val recFromCtx =
        Expressions.call(context, method, Expressions.constant(index))
      val recordAccess = recFromCtx
      var sType = storageType
      if (sType == null) {
        val fieldType = rowType.getFieldList.get(index).getType
        sType = typeFactory.getJavaClass(fieldType)
        if (Primitive.is(sType)) {}
      }

      Expressions.convert_(
        if (Primitive.is(sType)) {
          Expressions.convert_(recordAccess, Primitive.of(sType).boxClass)
        } else {
          recordAccess
        },
        sType
      )
    }
  }

  def getExecutable(
      rexBuilder: RexBuilder,
      exps: util.List[RexNode],
      rowType: RelDataType
  ): RexExecutable = {
    getExecutable(
      rexBuilder,
      exps,
      rowType,
      (rowType: RelDataType, typeFactory: JavaTypeFactory) =>
        new DataContextInputGetter(rowType, typeFactory)
    )
  }

  def getExecutable(
      rexBuilder: RexBuilder,
      exps: util.List[RexNode],
      rowType: RelDataType,
      getter: (RelDataType, JavaTypeFactory) => RexToLixTranslator.InputGetter
  ): RexExecutable = {
    val typeFactory = new JavaTypeFactoryImpl(
      rexBuilder.getTypeFactory.getTypeSystem
    )
    val code = logGeneratedCode(
      new RexExecutor().compile(
        rexBuilder,
        exps,
        getter(rowType, typeFactory),
        rowType
      )
    )

    new RexExecutable(code, "generated Rex code")
  }

  private def link[T: ClassTag](code: String): Any => HomogeneousColumn = {
    val cbe = new ClassBodyEvaluator
    cbe.setClassName("Evaluator")
    cbe.setExtendedClass(classOf[Utilities])
    cbe.setParentClassLoader(classOf[RexExecutable].getClassLoader)

    cbe.setImplementedInterfaces(
      Array[Class[_]](
        //            classOf[Function1[Any, Array[vt]]],
        classOf[Serializable]
      )
    )
    cbe.cook(new Scanner(null, new StringReader(code)))
    val c = cbe.getClazz
    //noinspection unchecked
    val constructor = c.getConstructor()
    val inst = constructor.newInstance()
    val method = inst.getClass.getMethod("evaluate", classOf[Any])
    (x: Any) =>
      toHomogeneousColumnOfKnownType[T](
        method
          .invoke(inst, x)
          .asInstanceOf[Array[T]]
      )
  }

  def getMapExecutable(
      rexBuilder: RexBuilder,
      exps: RexNode,
      rowType: RelDataType,
      isFilter: Boolean
  ): Any => HomogeneousColumn = {
    exps match {
      case ref: RexInputRef =>
        val ind = ref.getIndex
        return (tupleContext: Any) =>
          tupleContext.asInstanceOf[TupleContext].field2(ind)
      case _ =>
    }

    val getter = (rowType: RelDataType, typeFactory: JavaTypeFactory) =>
      new DataContextColumnGetter(rowType, typeFactory)

    val typeFactory = new JavaTypeFactoryImpl(
      rexBuilder.getTypeFactory.getTypeSystem
    )

    val (code, returnType) = compileMap(
      rexBuilder,
      exps,
      getter(rowType, typeFactory),
      rowType,
      isFilter
    )

    logGeneratedCode(code)

    try {
      if (Primitive.is(returnType)) {
        Primitive.of(returnType) match {
          case Primitive.BOOLEAN => link[Boolean](code)
          case Primitive.INT     => link[Int](code)
          case Primitive.LONG    => link[Long](code)
        }
      } else {
        Types.toClass(returnType) match {
          case x if x == classOf[java.math.BigDecimal] =>
            link[java.math.BigDecimal](code)
          case x if x == classOf[java.lang.Boolean] =>
            link[java.lang.Boolean](code)
          case x if x == classOf[java.lang.Integer] =>
            link[java.lang.Integer](code)
          case x if x == classOf[java.lang.Long] => link[java.lang.Long](code)
          case x if x == classOf[String]         => link[String](code)
        }
      }
    } catch {
      case e @ (_: CompileException | _: IOException |
          _: InstantiationException | _: IllegalAccessException |
          _: InvocationTargetException | _: NoSuchMethodException) =>
        throw new RuntimeException("While compiling reduction function", e)
    }
  }

  class DataContextColumnGetter(
      rowType: RelDataType,
      typeFactory: JavaTypeFactory
  ) extends RexToLixTranslator.InputGetter {
    def indices(index: Int): Expression = {
      val method = classOf[TupleContext].getMethod("indices", classOf[Int])
      val context =
        Expressions.convert_(DataContext.ROOT, classOf[TupleContext])
      Expressions.call(context, method, Expressions.constant(index))
    }

    override def field(
        list: BlockBuilder,
        index: Int,
        storageType: Type
    ): Expression = {
      //      val expr = DataContext.ROOT.asInstanceOf[TupleContext].fieldAsExpression(index)
      //      if (expr!= null) return expr

      val initialType = {
        val fieldType = rowType.getFieldList.get(index).getType
        typeFactory.getJavaClass(fieldType)
      }

      val sType =
        if (storageType == null) {
          initialType
        } else {
          storageType
        }

      val method = classOf[TupleContext].getMethod(
        if (Primitive.of(initialType) == Primitive.INT) {
          "field_int"
        } else if (Primitive.of(initialType) == Primitive.LONG) {
          "field_long"
        } else if (Primitive.of(initialType) == Primitive.BOOLEAN) {
          "field_boolean"
        } else if (initialType == classOf[java.math.BigDecimal]) {
          "field_bigdec"
        } else if (initialType == classOf[java.lang.String]) {
          "field_string"
        } else {
          "field2"
        },
        classOf[Int]
      )
      val context =
        Expressions.convert_(DataContext.ROOT, classOf[TupleContext])
      val recFromCtx =
        Expressions.call(context, method, Expressions.constant(index))
      val recordAccess = recFromCtx

      Expressions.convert_(
        recordAccess,
        new Types.ArrayType(sType)
      )
    }
  }

  def compileMap(
      rexBuilder: RexBuilder,
      constExps: RexNode,
      getter: RexToLixTranslator.InputGetter,
      rowType: RelDataType,
      isFilter: Boolean
  ): (String, Type) = {
    val programBuilder = new RexProgramBuilder(rowType, rexBuilder)
    if (isFilter) {
      programBuilder.addCondition(
        constExps
      )
    } else {
      programBuilder.addProject(
        constExps,
        "c" + programBuilder.getProjectList.size
      )
    }
    val javaTypeFactory = new JavaTypeFactoryImpl(
      rexBuilder.getTypeFactory.getTypeSystem
    )
    val blockBuilder = new BlockBuilder(false)
    val root0_ = Expressions.parameter(classOf[Any], "root0")
    val root_ = DataContext.ROOT
    blockBuilder.add(
      Expressions.declare(
        Modifier.FINAL,
        root_,
        Expressions.convert_(root0_, classOf[DataContext])
      )
    )
    val conformance = SqlConformanceEnum.DEFAULT
    val program = programBuilder.getProgram

    val columns = rowType.getFieldList.asScala.map(f => {
      Option.empty[ParameterExpression]
    })

    val index = Expressions.parameter(classOf[Int], "index")
    val inner = new BlockBuilder(true, blockBuilder)

    val innerGetter: RexToLixTranslator.InputGetter =
      (_: BlockBuilder, columnIndex: Int, storageType: Type) => {
        var sType = storageType
        if (sType == null) {
          val fieldType = rowType.getFieldList.get(columnIndex).getType
          sType = javaTypeFactory.getJavaClass(fieldType)

        }

        if (columns(columnIndex).isEmpty) {
          val f = rowType.getFieldList.get(columnIndex)
          val e = getter.field(blockBuilder, f.getIndex, null)
          val col =
            Expressions.parameter(
              e.getType,
              "col_" + f.getIndex + "_" + f.getName
                .replaceAll("[^a-zA-Z0-9]", "")
            )
          blockBuilder.add(
            Expressions.declare(
              Modifier.FINAL,
              col,
              getter.field(blockBuilder, f.getIndex, null)
            )
          )
          columns(columnIndex) = Option(col)
        }

        Expressions.arrayIndex(columns(columnIndex).get, index)
      }

    val expressions = if (isFilter) {
      RexToLixTranslator.translateCondition(
        program,
        javaTypeFactory,
        inner,
        innerGetter,
        null,
        conformance
      )
    } else {
      RexToLixTranslator
        .translateProjects(
          program,
          javaTypeFactory,
          conformance,
          inner,
          null,
          root_,
          innerGetter,
          null
        )
        .get(0)
    }

    val length = Expressions.parameter(classOf[Int], "length")
    blockBuilder.add(
      Expressions.declare(
        Modifier.FINAL,
        length, {
          val method = classOf[TupleContext].getMethod("size")
          val context =
            Expressions.convert_(DataContext.ROOT, classOf[TupleContext])
          Expressions.call(context, method)
        }
      )
    )

    val t = new Types.ArrayType(expressions.getType)
    val res = Expressions.parameter(t, "res")
    blockBuilder.add(
      Expressions.declare(
        0,
        res,
        Expressions.newArrayBounds(
          t,
          1,
          length
        ) //expressions.get(0).getType)
      )
    )

    inner.add(
      Expressions.statement(
        Expressions
          .assign(Expressions.arrayIndex(res, index), expressions)
      )
    )
    val loop = Expressions.for_(
      Expressions.declare(
        0,
        index,
        Expressions.constant(0)
      ),
      Expressions.lessThan(index, length),
      Expressions.preIncrementAssign(index),
      inner.toBlock
    )

    blockBuilder.add(loop)
    blockBuilder.add(
      Expressions.return_(
        null,
        res
      )
    )
    val methodDecl = Expressions.methodDecl(
      Modifier.PUBLIC,
      t,
      "evaluate",
      ImmutableList.of(root0_),
      blockBuilder.toBlock
    )

    (Expressions.toString(methodDecl), expressions.getType)
  }

  def createAggReduce(
      inputType: RelDataType
  ): (Any, Any) => Any = {
    val typeFactory = new JavaTypeFactoryImpl()
    val t = typeFactory.getJavaClass(inputType)

    val blockBuilder = new BlockBuilder()

    val v1 = Expressions.parameter(classOf[Any], "v1")
    val v2 = Expressions.parameter(classOf[Any], "v2")

    blockBuilder.add(
      Expressions.return_(
        null,
        if (Primitive.is(t)) {
          Expressions.add(
            Expressions.convert_(v1, Primitive.box(t)),
            Expressions.convert_(v2, Primitive.box(t))
          )
        } else {
          Expressions.call(
            classOf[SqlFunctions]
              .getMethod("plusAny", classOf[Any], classOf[Any]),
            Expressions.convert_(Expressions.convert_(v1, t), classOf[Any]),
            Expressions.convert_(Expressions.convert_(v2, t), classOf[Any])
          )
        }
      )
    )

    val methodDecl = Expressions.methodDecl(
      Modifier.PUBLIC,
      classOf[Any],
      "apply",
      util.List.of(v1, v2),
      blockBuilder.toBlock
    )

    val code =
      logGeneratedCode(Expressions.toString(methodDecl))

    try {
      val cbe = new ClassBodyEvaluator
      cbe.setClassName("Aggregator")
      cbe.setExtendedClass(classOf[Utilities])
      cbe.setImplementedInterfaces(
        Array[Class[_]](
          classOf[Function2[Any, Any, Any]],
          classOf[Serializable]
        )
      )
      cbe.setParentClassLoader(classOf[RexExecutable].getClassLoader)
      cbe.cook(new Scanner(null, new StringReader(code)))
      val c = cbe.getClazz.asInstanceOf[Class[_ <: Function2[Any, Any, Any]]]
      //noinspection unchecked
      val constructor = c.getConstructor()
      constructor.newInstance()
    } catch {
      case e @ (_: CompileException | _: IOException |
          _: InstantiationException | _: IllegalAccessException |
          _: InvocationTargetException | _: NoSuchMethodException) =>
        throw new RuntimeException("While compiling reduction function", e)
    }
  }

}
