/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.rel

import ch.epfl.dias.cs422.helpers.rel.RelOperator.{
  Elem,
  HomogeneousColumn,
  Tuple
}
import ch.epfl.dias.cs422.helpers.rel.late.RelOperatorLateUtil.VID
import ch.epfl.dias.cs422.helpers.rex.AggregateCall
import org.apache.calcite.rel.RelNode
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rex.{RexNode, RexUtil}

import scala.reflect.ClassTag

object RelOperator {
  final type Elem = Any
  final type Tuple = IndexedSeq[Elem]
  final type Column = IndexedSeq[Elem]
  /**
   * A Column whose elements are all known to be of the same type.
   */
  final type HomogeneousColumn =
    scala.Either[scala.Either[scala.Either[Array[java.math.BigDecimal], Array[
      String
    ]], scala.Either[Array[Long], Array[java.lang.Long]]], scala.Either[
      scala.Either[Array[Int], Array[java.lang.Integer]],
      scala.Either[Array[Boolean], Array[java.lang.Boolean]]
    ]]

  /**
   * Returns the size of a HomogeneousColumn
   *
   * @param col Column whose elements to count
   * @return Number of elements in the column
   */
  implicit def size(col: HomogeneousColumn): Int = {
    def s[T](something: T): Int = {
      something match {
        case e: Either[_, _] =>
          e.fold(
            a => s(a),
            b => s(b)
          )
        case a: Array[_] => a.length
      }
    }

    s(col)
  }

  /**
   * Unwraps a HomogeneousColumn of known type T, into an
   * array of the same type.
   *
   * Use this function when you know the element types (ie. Boolean)
   * and you want to recover the type-annotated column
   */
  def unwrap[T](col: HomogeneousColumn): Array[T] = {
    def s[K](something: K): Array[T] = {
      something match {
        case e: Either[_, _] =>
          e.fold(
            a => s(a),
            b => s(b)
          )
        case a: Array[T] => a
      }
    }

    s(col)
  }

  /**
   * Converts a HomogeneousColumn into an Iterable.
   */
  implicit def asIterable(col: HomogeneousColumn): Iterable[Any] = {
    def s[T](something: T): Iterable[Any] = {
      something match {
        case e: Either[_, _] =>
          e.fold(
            a => s(a),
            b => s(b)
          )
        case a: Array[_] => a
      }
    }

    s(col)
  }

  private def asHomogeneousColumnFromArray(f: Array[_]): HomogeneousColumn = {
    f match {
      case fa: Array[java.math.BigDecimal] => Left(Left(Left(fa)))
      case fa: Array[String]               => Left(Left(Right(fa)))
      case fa: Array[Long]                 => Left(Right(Left(fa)))
      case fa: Array[java.lang.Long]       => Left(Right(Right(fa)))
      case fa: Array[Int]                  => Right(Left(Left(fa)))
      case fa: Array[java.lang.Integer]    => Right(Left(Right(fa)))
      case fa: Array[Boolean]              => Right(Right(Left(fa)))
      case fa: Array[java.lang.Boolean]    => Right(Right(Right(fa)))
    }
  }

  /**
   * Converts the input Array into a HomogeneousColumn of type T.
   *
   * This is an expensive operation. Avoid as much as possible.
   */
  def toHomogeneousColumnOfKnownType[T: ClassTag](
      f: Column
  ): HomogeneousColumn = {
    asHomogeneousColumnFromArray(f.toArray.map(e => e.asInstanceOf[T]))
  }

  /**
   * Converts the input Array into a HomogeneousColumn of type T.
   */
  def toHomogeneousColumn[T: ClassTag](f: Array[T]): HomogeneousColumn = {
    asHomogeneousColumnFromArray(f)
  }

  /**
   * Tries to autodetect the Column's elements based on the first element
   * and then converts the column to a HomogeneousColumn of that type.
   *
   * This is an expensive operation. Avoid as much as possible.
   */
  implicit def toHomogeneousColumn(f: Column): HomogeneousColumn = {
    f.headOption.getOrElse(0) match {
      case _: Int     => toHomogeneousColumnOfKnownType[Int](f)
      case _: Integer => toHomogeneousColumnOfKnownType[Integer](f)
      case _: Long    => toHomogeneousColumnOfKnownType[Long](f)
      case _: java.lang.Long =>
        toHomogeneousColumnOfKnownType[java.lang.Long](f)
      case _: Boolean => toHomogeneousColumnOfKnownType[Boolean](f)
      case _: java.lang.Boolean =>
        toHomogeneousColumnOfKnownType[java.lang.Boolean](f)
      case _: java.math.BigDecimal =>
        toHomogeneousColumnOfKnownType[java.math.BigDecimal](f)
      case _: String => toHomogeneousColumnOfKnownType[String](f)
    }
  }

  /** Tuple that represents a late materialized row. It contains a VID and a Tuple of attributes
    * that have already been materialized
    *
    * @param vid  Virtual object/row ID, starting from 0 for the first row and incrementing by one
    * @param value Materialized part of the tuple for this vid
    */
  case class LateTuple(vid: VID, value: Tuple) {}

  /** Entry that represents a range from startVID to endVID (both inclusive) with the same value
    *
    * @param startVID  Virtual object/row ID, starting from 0 for the first row and incrementing by one
    * @param length    Number of elements covered by the entry
    * @param value     Common value across the entries
    */
  case class RLEentry(startVID: VID, length: Long, value: Tuple) {
    def endVID: VID = startVID + length - 1
  }

  final type RLEColumn = IndexedSeq[RLEentry]
  final type Block = IndexedSeq[Tuple]
  final type PAXMinipage =
    IndexedSeq[Elem] // like a column but with maximum length of X elements
  final type PAXPage = IndexedSeq[PAXMinipage]

  /**
    * A tuple that signifies the end of stream
    */
  final val NilTuple: Option[Nothing] = None
  /**
   * An RLE tuple that signifies the end of stream
   */
  final val NilRLEentry:  Option[Nothing] = None
  /**
    * A Late materialization tuple that signifies the end of stream
    */
  final val NilLateTuple: Option[Nothing] = None

  final val blockSize: Int = 4
}

trait RelOperator extends RelNode {

  /** Produces a function that evaluates a RexNode on a tuple.
    *
    * Produces the results of expression `e`, applied to an input tuple `input`.
    * Example usage: {{{ eval(cond, inputTupleType)(tuple) }}}
    *
    * @param e            Tuple-level expression to evaluate
    * @param inputRowType Type of expected input tuple for the expression evaluation
    * @return Returns a function that accepts a tuple as and returns the result of expression @p e.
    * @group helpers
    */
  final def eval(e: RexNode, inputRowType: RelDataType): Tuple => Elem = {
    RelOperatorUtil.eval(getCluster, e, inputRowType)
  }

  /** Produces a function that evaluates a list of RexNodes on a tuple, producing another tuple
    *
    * Produces the results of expressions `e`, applied to an input tuple `input`.
    * Example usage:
    * eval(IndexedSeq(cond), inputTupleType)(tuple)
    *
    * @param e            Tuple-level expression to evaluate
    * @param inputRowType Type of expected input tuple for the expression evaluation
    * @return Returns a function that accepts a tuple as and returns the result of expression `e`.
    * @group helpers
    */
  final def eval(
      e: IndexedSeq[RexNode],
      inputRowType: RelDataType
  ): Tuple => Tuple = {
    RelOperatorUtil.eval(getCluster, e, inputRowType)
  }

  /** Produces a function that evaluates a RexNodes on a list of HomogeneousColumn, producing another HomogeneousColumn
    *
    * Produces the results of expressions `e`, applied to an input table `input`.
    * Example usage:
    * eval(cond, inputTupleType)(columns)
    *
    * @param e            Tuple-level expression to evaluate
    * @param inputRowType Type of expected input tuple for the expression evaluation
    * @return Returns a function that accepts a list of HomogeneousColumn and returns the result of expression `e`
    *         as a new HomogeneousColumn.
    * @group helpers
    */
  final def map(
      e: RexNode,
      inputRowType: RelDataType,
      isFilterCondition: Boolean
  ): IndexedSeq[HomogeneousColumn] => HomogeneousColumn = {
    RelOperatorUtil.map(getCluster, e, inputRowType, isFilterCondition)
  }

  /**
    * Evaluates a literal expression. That is, a RexNode that always returns the same constant value and does not require
    * any input.
    *
    * @param e a literal expression
    * @return The evaluated expression
    * @group helpers
    */
  final def evalLiteral(e: RexNode): Any = {
    assert(RexUtil.isLiteral(e, true), "evalLiteral only accepts literals")
    RelOperatorUtil.eval(
      getCluster,
      e,
      getRowType /* we just need some type, doesn't matter which */
    )(null)
  }

  /**
    * Alias for [[ch.epfl.dias.cs422.helpers.rex.AggregateCall#reduce]](e1, e2)
    *
    * @param e1      first partial result
    * @param e2      second partial result
    * @param aggCall aggregation function
    * @return A partial result that combines e1 with e2 using the aggregate function aggCall
    * @group helpers
    */
  final def aggReduce(e1: Elem, e2: Elem, aggCall: AggregateCall): Elem =
    aggCall.reduce(e1, e2)

  /**
    * Alias for [[ch.epfl.dias.cs422.helpers.rex.AggregateCall#emptyValue]]
    *
    * @param aggCall aggregation function
    * @return Value of aggregate function when the input set is empty
    * @group helpers
    */
  final def aggEmptyValue(aggCall: AggregateCall): Elem = aggCall.emptyValue

  /**
    * Returns the type of the (logical) tuples produced by this operator
    *
    * @group helpers
    */
  def getRowType: RelDataType // only here to change documentation group
}
