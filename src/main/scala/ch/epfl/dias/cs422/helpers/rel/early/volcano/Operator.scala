/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.rel.early.volcano

import ch.epfl.dias.cs422.helpers.rel.RelOperator
import ch.epfl.dias.cs422.helpers.rel.RelOperator.Tuple

trait Operator extends RelOperator with Iterable[Tuple] {
  /** Resets Operator to the initial state.
    *
    * Called exactly once before any calls to [[next]] function.
    *
    * @group helpers
    */
  def open(): Unit

  /** Returns the next tuple produced by the operator.
    *
    * Return a tuple as a [[Tuple]] ([[IndexedSeq]]) of attributes, or [[RelOperator.NilTuple]]
    * to signify that there are no more available tuples.
    * The order and type of the [[IndexedSeq]] elements conforms to getRowType.
    * After [[next]] returns [[RelOperator.NilTuple]], calling [[next]] again has undefined behavior
    * and the Operator may logically enter a state that only accepts calls
    * to [[close]] and [[open]].
    *
    * @return Next tuple as an [[IndexedSeq]] of attributes,
    *         or [[RelOperator.NilTuple]]
    *         for end of stream.
    *
    * @group helpers
    */
  def next(): Option[Tuple]

  /** Allows the Operator to clean up its resources.
    *
    * Every call to [[open]] should be matched to a call to [[close]] and vice versa.
    * Furthermore, after a call to [[close]], the Operator will only accept calls to [[open]].
    * Calling [[next]] after [[close]], without re-[[open]]ing the operator,
    * has undefined behavior.
    *
    * @group helpers
    */
  def close(): Unit

  /** Returns an iterator to iterate over the tuples produced by the operator.
    *
    * The iterator takes care of opening and closing the operator.
    *
    * @return Iterator over the tuples produced by the current operator.
    *
    * @group helpers
    */
  final override def iterator: Iterator[Tuple] =
    new Iterator[Tuple] with AutoCloseable {
      private val op =
        Operator.this.copy(getTraitSet, getInputs).asInstanceOf[Operator]
      op.open()

      var n: Option[Option[Tuple]] = Option.empty

      def prepareNext(): Unit = {
        if (n.nonEmpty) return
        n = Option(op.next())
      }

      override def hasNext: Boolean = {
        prepareNext()
        n.get.nonEmpty
      }

      override def next(): Tuple = {
        prepareNext()
        val ret = n.get
        assert(ret.nonEmpty)
        n = Option.empty
        ret.get
      }

      override def close(): Unit = {
        op.close()
      }
    }
}
