/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.builder.skeleton

import ch.epfl.dias.cs422.helpers.Construct
import ch.epfl.dias.cs422.helpers.rel.RelOperator
import org.apache.calcite.plan.RelTraitSet
import org.apache.calcite.rel.{RelCollation, RelNode, core}
import org.apache.calcite.rex.{RexLiteral, RexNode}

/**
  * The Sort operator performs two operations: it both sorts the input as well as it slices it.
  * This allows for optimizing the operation using for example min-heaps.
  *
  * Specifically, it is responsible for the Order By clause, as well as the SQL clause `OFFSET offset FETCH NEXT fetch`.
  * The [[collation]] field describes the ordering and the [[offset]]/[[fetch]] specify the optional slicing parameters.
  *
  * Overall, the Sort operator sorts its input using a key created based on the collation and AFTER sorting
  * it drops `offset` tuples and returns the next `fetch` tuples. Note that the AFTER is the
  * interface requirement, but optimized implimentations can use the offset and fetch to avoid sorting the
  * full input, for example if Sort is interested only for the top 101-200 elements.
  *
  * Regarding the ordering key and direction, the
  * [[org.apache.calcite.rel.RelCollation#getFieldCollations collation.getFieldCollations]] returns the collations as a
  * list of RelFieldCollation `s` and you can use this list to create the sorting key.
  * To compare two equally-sized lists, compare their elements pair-wise and use comparisons deeper in the list as tie-breakers.
  *
  * The i-th RelFieldCollation specifies the input field that it references
  * (j = [[org.apache.calcite.rel.RelFieldCollation#getFieldIndex RelFieldCollation.getFieldIndex]]) and
  * the sort direction (RelFieldCollation.direction.isDescending).
  *
  * Thus if a collation.getFieldCollations returns:
  *   List( {index=3, direction.isDescending=true}, {index=0, direction.isDescending=true} ),
  * the key for an input tuple A=(A0, A1, A2, A3, A4) is (A3 DESC, A0 ASC) and the tuples you
  * should return have the same format as A.
  *
  * Note that any input element referenced
  * by [[org.apache.calcite.rel.RelFieldCollation#getFieldIndex RelFieldCollation.getFieldIndex]]
  * is guaranteed to be [[java.lang.Comparable Comparable[Elem] ]]
  *
  * To implement the OFFSET operation, from the sorted output you drop the first `offset` tuples.
  *
  * To implement the FETCH NEXT/FIRST, LIMIT operation, from the sorted output, after you drop offset elements,
  * take only `fetch` tuples.
  *
  *
  * @param input     Input operator
  * @param collation Specified the order for the output tuples (aka, it describes the "key").
  * @param offset    Number of rows to discard before returning first row, or [[None]] to discard nothing
  * @param fetch     Expression for number of rows to fetch, or [[None]] to fetch all
  *
  * @see [[ch.epfl.dias.cs422.helpers.rel.RelOperator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.late.LazyOperator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.early.blockatatime.Operator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.early.operatoratatime.Operator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.early.volcano.Operator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.late.operatoratatime.Operator]]
  * @see [[org.apache.calcite.rel.core.Sort]]
  *
  * @group helpers
  */
abstract class Sort[TOperator <: RelNode] protected (
    input: TOperator,
    collation: RelCollation,
    offset: Option[Int],
    fetch: Option[Int]
) extends core.Sort(
      input.getCluster,
      input.getTraitSet.replace(collation),
      input,
      collation,
      input.getCluster.getRexBuilder
        .makeExactLiteral(java.math.BigDecimal.valueOf(offset.getOrElse(0))),
      if (fetch.nonEmpty)
        input.getCluster.getRexBuilder
          .makeExactLiteral(java.math.BigDecimal.valueOf(fetch.get))
      else null
    ) {
  self: TOperator =>

  override def copy(
      traitSet: RelTraitSet,
      newInput: RelNode,
      newCollation: RelCollation,
      offset: RexNode,
      fetch: RexNode
  ): Sort[TOperator] = {
    Sort.create(
      newInput.asInstanceOf[TOperator],
      newCollation,
      if (offset == null) Option.empty[Int]
      else
        Option[Int](
          offset
            .asInstanceOf[RexLiteral]
            .getValueAs(classOf[Integer])
            .asInstanceOf[Int]
        ),
      if (fetch == null) Option.empty[Int]
      else
        Option[Int](
          fetch
            .asInstanceOf[RexLiteral]
            .getValueAs(classOf[Integer])
            .asInstanceOf[Int]
        ),
      this.getClass
    )
  }
}

private[helpers] object Sort {
  def create[TOperator <: RelNode](
      child: TOperator,
      collation: RelCollation,
      offset: Option[Int],
      fetch: Option[Int],
      c: Class[_ <: Sort[TOperator]]
  ): Sort[TOperator] = {
    Construct.create(c, child, collation, offset, fetch)
  }
}
