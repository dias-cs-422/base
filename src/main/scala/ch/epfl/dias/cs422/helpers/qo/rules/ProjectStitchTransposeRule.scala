package ch.epfl.dias.cs422.helpers.qo.rules

import ch.epfl.dias.cs422.helpers.builder.skeleton.logical.LogicalStitch
import org.apache.calcite.plan.RelOptRule.{any, operand}
import org.apache.calcite.plan.{RelOptRule, RelOptRuleCall}
import org.apache.calcite.rel.RelNode
import org.apache.calcite.rel.core.Project
import org.apache.calcite.rex.RexNode

class ProjectStitchTransposeRule
  extends RelOptRule(
    // Match Project(logical.LogicalStitch(any...))
    operand(classOf[Project], operand(classOf[LogicalStitch], any)),
    "ProjectStitchTransposeRule"
  ) {

  override def onMatch(call: RelOptRuleCall): Unit = {
    val proj: Project = call.rel(0)
    val recons: LogicalStitch = call.rel(1)
    val pushProject = new PushProjector(
      proj,
      null,
      recons,
      (expr: RexNode) => true,
      call.builder
    )

    if (pushProject.locateAllRefs) {
      return
    }

    val leftProjRel = pushProject
      .createProjectRefsAndExprs(recons.getLeft, true, false)
    val rightProjRel = pushProject
      .createProjectRefsAndExprs(recons.getRight, true, true)

    val topProject: RelNode = pushProject.createNewProject(
      LogicalStitch.create(
        leftProjRel,
        rightProjRel
      ),
      pushProject.getAdjustments
    )

    call.transformTo(topProject)
  }
}

