/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.rex

import org.apache.calcite.rel.core
import org.apache.calcite.runtime.SqlFunctions
import org.apache.calcite.sql.SqlKind
import org.apache.calcite.sql.fun._

import scala.jdk.CollectionConverters._

/**
  * Represents an Aggregation function, like MIN(x), COUNT(*), SUM(y), COUNT(z).
  *
  * In the above examples, single values of `x`, `1`, `y` and `z` are called "input arguemtns" and are given using the `getArgument(tuple)` of the corresponding Aggregation function.
  * `reduce` combines two input arguments into a partial result, which can be used as an input argument to another reduction
  * or as final result.
  *
  * If the group contains only one element, the result is the input argument itself (getArgument).
  */
final case class AggregateCall(private[helpers] val agg: core.AggregateCall) {
  lazy val redFun: (Any, Any) => Any = {
    RexExecutor.createAggReduce(agg.getType)
  }

  /**
    * Given two input arguments (see [[ch.epfl.dias.cs422.helpers.rex.AggregateCall#getArgument]]), it combines them
    * based on the aggregation function.
    *
    * For example, for `SELECT COUNT(*), SUM(x) FROM A GROUP BY y;` and `e1`=4, `e2`=`1`, there are two AggregateCalls:
    *   - The call for the `COUNT(*)`, whose `reduce(e1, e2)` returns `5`
    *   - The call for the `MIN(x)`, whose `reduce(e1, e2)` returns `1`
    *
    * Note that passing emptyValue into the reduce has an undefined behavior (eg. `reduce(e1, emptyValue)` is undefined is undefined).
    * To produce the result of an AggregateCall on a ''single'' input argument, return the input argument directly.
    *
    * @param e1 First input argument
    * @param e2 Second input argument
    * @return The result of combining e1 and e2 through the aggregation function.
    */
  def reduce(e1: Any, e2: Any): Any = {
    agg.getAggregation match {
      case _: SqlSumEmptyIsZeroAggFunction => redFun(e1, e2)
      case _: SqlSumAggFunction            => redFun(e1, e2)
      case _: SqlCountAggFunction          => redFun(e1, e2)
      case a: SqlMinMaxAggFunction =>
        val comp = e1 match {
          case b1: Boolean => b1 || !e2.asInstanceOf[Boolean]
          case _           => SqlFunctions.geAny(e1, e2)
        }
        if ((a.getKind == SqlKind.MAX) == comp) e1
        else e2
      case _: SqlAvgAggFunction => assert(false) // Should never happen
    }
  }

  /**
    * Given a tuple, it returns the input argument for the aggregation.
    *
    * For example, for `SELECT COUNT(*), SUM(x) FROM A GROUP BY y;`, there are two AggregateCalls:
    *   - The call for the `COUNT(*)`, whose `getArgument(tuple)` returns `1`
    *   - The call for the `SUM(x)`, whose `getArgument(tuple)` returns `tuple.x` (that is, if `x` is the 2nd field, it returns `tuple(1)` )
    *
    * If the group contains a single tuple, then getArgument is also the final aggregation result.
    *
    * @param e Input tuple
    * @return Input argument for the aggregation
    */
  def getArgument[Tuple <: IndexedSeq[Any]](e: Tuple): Any = {
    agg.getArgList.asScala.toList match {
      case x :: Nil => e(x)
      case Nil      => 1.asInstanceOf[Long]
      case _ =>
        new IllegalArgumentException("Unexpected arg list with size >= 2")
    }
  }

  /**
    * Given a tuple, it returns the input argument for the aggregation, reduced
    * as if it appeared `repeat` times.
    *
    * For example, for `SELECT COUNT(*), SUM(x) FROM A GROUP BY y;`, there are two AggregateCalls and `repeat` = 1:
    *   - The call for the `COUNT(*)`, whose `getArgument(tuple)` returns `1`
    *   - The call for the `SUM(x)`, whose `getArgument(tuple)` returns `tuple.x` (that is, if `x` is the 2nd field, it returns `tuple(1)` )
    *   - The call for the `MIN(x)`, whose `getArgument(tuple)` returns `tuple.x`
    *
    * If `repeat` equals 4, the above then:
    *   - The call for the `COUNT(*)`, whose `getArgument(tuple)` returns `4`
    *   - The call for the `SUM(x)`, whose `getArgument(tuple)` returns `4 * tuple.x` (that is, if `x` is the 2nd field, it returns `tuple(4)` )
    *   - The call for the `MIN(x)`, whose `getArgument(tuple)` returns `tuple.x`
    *
    * If the group contains a single tuple, then getArgument is also the final aggregation result.
    *
    * @param e Input tuple
    * @param repeat Number of repetitions of Tuple e
    * @return Input argument for the aggregation
    */
  def getArgument[Tuple <: IndexedSeq[Any]](e: Tuple, repeat: Long): Any = {
    val v = getArgument(e)
    agg.getAggregation match {
      case _: SqlSumEmptyIsZeroAggFunction =>
        v match {
          case _: java.math.BigDecimal => SqlFunctions.multiplyAny(v, repeat)
          case n: Int =>
            SqlFunctions.multiply(n.asInstanceOf[Integer], repeat.toInt).toInt
        }
      case _: SqlSumAggFunction =>
        v match {
          case _: java.math.BigDecimal => SqlFunctions.multiplyAny(v, repeat)
          case n: Int =>
            SqlFunctions.multiply(n.asInstanceOf[Integer], repeat.toInt).toInt
        }
      case _: SqlCountAggFunction  => repeat.toLong
      case _: SqlMinMaxAggFunction => v
    }
  }

  /**
    * Returns the value of the aggregate when the group is empty.
    *
    * Note that the group of the aggregate is ''not'' a value that can be used to bootstrap the reduction and the
    * [[ch.epfl.dias.cs422.helpers.rex.AggregateCall#reduce]] functions may not accept an emptyValue as an input.
    *
    * An empty group will not produce an output during an aggregation, but will produce an output during the a simple
    * reduction (no group by clause) (eg. `SELECT COUNT(*), SUM(x) FROM A WHERE false;` which results to `{(0, NULL)}`)
    *
    * For example, for `SELECT COUNT(*), SUM(x) FROM A`, there are two AggregateCalls:
    *   - The call for the `COUNT(*)`, whose `emptyValue` returns `0`
    *   - The call for the `SUM(x)`, whose `emptyValue` returns `null`
    *
    * @return Value of aggregate call when the group is empty
    */
  def emptyValue: Any = {
    agg.getAggregation match {
      case _: SqlSumEmptyIsZeroAggFunction => 0
      case _: SqlSumAggFunction            => null
      case _: SqlCountAggFunction          => 0
      case _: SqlMinMaxAggFunction         => null
    }
  }
}
