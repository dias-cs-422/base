package ch.epfl.dias.cs422.helpers.qo.rules.skeleton

import ch.epfl.dias.cs422.helpers.builder.skeleton.logical.LogicalFetch
import org.apache.calcite.plan.{RelOptRuleCall, RelRule}
import org.apache.calcite.rel.RelNode
import org.apache.calcite.rel.logical.LogicalFilter

/**
  * RelRule (optimization rule) that finds a reconstruct operator that
  * stitches a filtered column (scan then filter) with the late materialized
  * tuple and transforms stitching into a fetch operator followed by a filter.
  *
  * To use this rule: LazyFetchProjectRule.Config.DEFAULT.toRule()
  *
  * @param config configuration parameters of the optimization rule
  */
class LazyFetchFilterRuleSkeleton protected (config: RelRule.Config)
  extends RelRule(
    config
  ) {

  def onMatchHelper(call: RelOptRuleCall): RelNode = ???

  override final def onMatch(call: RelOptRuleCall): Unit = {
    val result = onMatchHelper(call)

    assert(result.isInstanceOf[LogicalFilter])
    val child = result.getInput(0)
    assert(child.isInstanceOf[LogicalFetch])

    call.transformTo(
      result
    )
  }
}