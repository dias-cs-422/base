/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.builder.skeleton

import ch.epfl.dias.cs422.helpers.Construct
import ch.epfl.dias.cs422.helpers.store.{ScannableTable, Store}
import org.apache.calcite.plan.{RelOptCluster, RelOptTable, RelTraitSet}
import org.apache.calcite.rel.RelNode
import org.apache.calcite.rel.core.TableScan

/** Reads the tuples from the Store and returns them to the query.
  *
  * @group helpers
  */
abstract class Scan[TOperator <: RelNode] protected (
    cluster: RelOptCluster,
    traitSet: RelTraitSet,
    table: RelOptTable
) extends TableScan(cluster, traitSet, table) {
  self: TOperator =>
}

private[helpers] object Scan {
  def create[TOperator <: RelNode](
      cluster: RelOptCluster,
      table: RelOptTable,
      c: Class[_ <: Scan[TOperator]],
      tableToStore: ScannableTable => Store
  ): Scan[TOperator] =
    Construct.create(c, cluster, cluster.traitSet(), table, tableToStore)
}
