/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.rel.late

import org.apache.calcite.rel.RelNode
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rex.{RexInputRef, RexNode}

import scala.jdk.CollectionConverters._

/**
 * Evaluator that through a list of functions, provides access to materialized storage.
 *
 * @param l A List of functions. The i-th function, given an input VID should produce an the i-th element of the tuple
 *          referenced by the VID.
 *
 * @group helpers
 */
class LazyEvaluatorAccess(val l: List[Long => Any]) extends LazyEvaluatorRoot {
  def apply(vs: IndexedSeq[Any]): IndexedSeq[Any] = {
    assert(vs.size == 1)
    l.toIndexedSeq.map(f => f(vs.head.asInstanceOf[Long]))
  }

  def apply(vs: Long): IndexedSeq[Any] = {
    l.toIndexedSeq.map(f => f(vs))
  }
}

/**
 * LazyOperators produce VIDs instead of actual tuples. VIDs are either single VID objects or Lists of VIDs.
 * Thus, an operator may produce the following output structure:
 * {{{
 * VIDs = IndexedSeq[ VID, IndexedSeq[ IndexedSeq[ IndexedSeq[VID] ], VID] ]
 * }}}
 *
 * RexNodes represent expressions that are evaluated on input tuples.
 *
 * For late materialization, the input tuples are not materialized and thus operators have to notify their parents
 * how to materialize them, if needed.
 * For example, a `Project(a=x + 3)` has to tell its parent that to compute `a` it should read x and add 3.
 *
 * More specifically, operators expose information for their output through [[ch.epfl.dias.cs422.helpers.rel.late.LazyOperator#evaluators]].
 * [[ch.epfl.dias.cs422.helpers.rel.late.LazyOperator#evaluators]] describes how the output tuple is to be computed, when
 * needed. If an operator produces an output `v: VIDs`, then the parent operator can materialize the output tuple of
 * its child by calling `child.evaluators(v)`.
 *
 *
 * Evaluators can be combined through
 * [[ch.epfl.dias.cs422.helpers.rel.late.LazyOperator#lazyEval(scala.collection.immutable.IndexedSeq,org.apache.calcite.rel.type.RelDataType,ch.epfl.dias.cs422.helpers.rel.late.LazyEvaluatorRoot) lazyEval(1)]]
 * and
 * [[ch.epfl.dias.cs422.helpers.rel.late.LazyOperator#lazyEval(ch.epfl.dias.cs422.helpers.rel.late.LazyEvaluatorRoot,ch.epfl.dias.cs422.helpers.rel.late.LazyEvaluatorRoot,org.apache.calcite.rel.type.RelDataType,org.apache.calcite.rel.type.RelDataType) lazyEval(2)]]:
 * The `Project(a=x + 3)` of the previous example references `x`, which is not materialized. lazyEval allows creating
 * new evaluators, using the evaluators of the input(s).
 *
 * Operators whose output is accessing materialized results (eg. Scan) can use [[ch.epfl.dias.cs422.helpers.rel.late.LazyEvaluatorAccess]] to provide access
 * to the materialized tuples.
 *
 * @group helpers
 */
trait LazyOperator extends RelNode {
  type VID = RelOperatorLateUtil.VID
  type VIDs = RelOperatorLateUtil.VIDs

  /**
   * Creates an evaluators for expression e. The input row conforms to type inputRowType and is constructed through
   * the evaluators.
   * The expected VID input is the same as the input for the evaluators.
   *
   * See LazyEval
   *
   * @group helpers
   */
  def eval(e: RexNode, inputRowType: RelDataType, evaluators: LazyEvaluatorRoot): Evaluator = {
    RelOperatorLateUtil.eval(getCluster, e, inputRowType, evaluators)
  }

  /**
   * Given a LazyEvaluatorRoot that results on a row type rowType, create a new lazy expression that only evaluates
   * the fields pointed by the indices in the ind array
   *
   * @group helpers
   */
  def indices(ind: IndexedSeq[Int], rowType: RelDataType, eval: LazyEvaluatorRoot): LazyEvaluator = {
    val lk = ind.map(i => new RexInputRef(i, rowType.getFieldList.get(i).getType))
    lazyEval(lk, rowType, eval)
  }

  /**
   * Given a IndexedSeq of expressions, it produces the evaluators for them. The input is assumed to be coming form the
   * `evaluators` input and the evaluators should produce a row of inputRowType format.
   *
   * More specifically, returns the description for computing the given RexNodes (`e`) on top of a tuple of type `inputRowType`.
   * `evaltors` are assumed to be the evaluators that, if needed, will materialize the input tuples.
   *
   * For example, to create the `evaluators()` for a `Project(a=x + 3)`, `evaluators` would map to the project's
   * input (that includes `x`), inputRowType would provide the output type of the project's input and
   * `RexNodes = IndexedSeq("x+3")`.
   *
   * If `evaluators` was expecting as input VIDs in the a format F (eg. (VID, (VID, VID)), then the new evaluators would
   * expect the same input format.
   *
   * @param e            Expressions to convert into evaluators
   * @param inputRowType Type of the tuple expected by the expressions in `e`
   * @param evaluators   Evaluators that compute the input tuples expected by `e`. They should produce a tuple of
   *                     `inputRowType` type
   * @return evaluators for the expression `e` that accept the same VIDs format as `e`
   *
   * @group helpers
   */
  def lazyEval(e: IndexedSeq[RexNode], inputRowType: RelDataType, evaluators: LazyEvaluatorRoot): LazyEvaluator = {
    RelOperatorLateUtil.lazyEval(getCluster, e, inputRowType, evaluators)
  }

  /**
   * Concatenates two evaluators
   *
   * Create a new evaluator that combines the left and right evaluators to produce a tuple. The new input is expected
   * to be an IndexedSeq with two VIDs, one for each side. (eg. IndexedSeq(5, 10) or IndexedSeq(IndexedSeq(5, 10), 25) )
   *
   * If the input evaluators expect VIDs in format F1 and F2 respectively, the new evaluator will expect (F1, F2).
   *
   * @param leftEval     first set of outputs
   * @param rightEval    second set of outputs
   * @param leftRowType  output type of leftEval
   * @param rightRowType output type of rightEval
   * @return The concatenation of leftEval :: rightEval
   *
   * @group helpers
   */
  def lazyEval(leftEval: LazyEvaluatorRoot, rightEval: LazyEvaluatorRoot, leftRowType: RelDataType, rightRowType: RelDataType): LazyEvaluator = {
    val rowTypeList = leftRowType.getFieldList.asScala ++ rightRowType.getFieldList.asScala
    val rowType = getCluster.getTypeFactory.createStructType(rowTypeList.map(f => f.getType).asJava, rowTypeList.map(f => f.getName).asJava)

    val fs = rowType.getFieldList.asScala.zipWithIndex.map(e => new RexInputRef(e._1.getIndex, e._1.getType)).toIndexedSeq
    lazyEval(fs, rowType, new MergeLazyEvaluator(leftEval, rightEval, List(leftRowType, rightRowType)))
  }

  /**
   * Return the LazyEvaluator that evaluates the output tuple of this operator.
   *
   * A parent operator would be able to get a materialized version of the tuple by invoking `input.evaluators()(vids)`,
   * where `vids` is one of the tuples generated by the input operator.
   *
   * The return value should conform to the type of [[org.apache.calcite.rel.RelNode#getRowType]]
   *
   * @return the LazyEvaluator that evaluates the output tuple of this operator.
   *
   * @group helpers
   */
  def evaluators(): LazyEvaluatorRoot
}
