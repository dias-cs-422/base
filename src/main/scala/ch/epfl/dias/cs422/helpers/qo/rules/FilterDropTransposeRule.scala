package ch.epfl.dias.cs422.helpers.qo.rules

import ch.epfl.dias.cs422.helpers.builder.skeleton.logical.LogicalDrop
import org.apache.calcite.plan.{RelOptRuleCall, RelRule}
import org.apache.calcite.rel.logical.LogicalFilter

/**
  * RelRule (optimization rule) that finds a filter above a decode
  * and pushes it bellow it.
  *
  * To use this rule: FilterDecodeTransposeRule.Config.DEFAULT.toRule()
  *
  * @param config configuration parameters of the optimization rule
  */
class FilterDropTransposeRule protected (config: RelRule.Config)
  extends RelRule(config) {

  override def onMatch(call: RelOptRuleCall): Unit = {
    val filter: LogicalFilter = call.rel(0)
    val drop: LogicalDrop = call.rel(1)

    call.transformTo(
      drop.copy(
        filter.copy(filter.getTraitSet, drop.getInput, filter.getCondition)
      )
    )
  }
}

object FilterDropTransposeRule {

  /**
    * Configuration for a [[FilterDecodeTransposeRule]]
    */
  val INSTANCE = new FilterDropTransposeRule(
    // By default, get an empty configuration
    RelRule.Config.EMPTY
      // and match:
      .withOperandSupplier((b: RelRule.OperandBuilder) =>
        // A node of class classOf[LogicalFilter]
        b.operand(classOf[LogicalFilter])
          // that has inputs:
          .oneInput(b1 =>
            // A node that is a LogicalDecode
            b1.operand(classOf[LogicalDrop])
              // of any inputs
              .anyInputs()
          )
      )
  )
}
