package ch.epfl.dias.cs422.helpers.builder.skeleton

import ch.epfl.dias.cs422.helpers.Construct
import org.apache.calcite.plan.RelTraitSet
import org.apache.calcite.rel.RelNode

import java.util

/**
  * @inheritdoc
  *
  * @see [[ch.epfl.dias.cs422.helpers.builder.skeleton.logical.LogicalStitch]]
  */
class Stitch[TOperator <: RelNode] protected (
                                                    left: TOperator,
                                                    right: TOperator
                                                  ) extends logical.LogicalStitch(left, right) {

  override def copy(
                     traitSet: RelTraitSet,
                     inputs: util.List[RelNode]
                   ): Stitch[TOperator] = {
    assert(inputs.size() == 2)
    copy(
      inputs.get(0).asInstanceOf[TOperator],
      inputs.get(1).asInstanceOf[TOperator]
    )
  }

  def copy(
            left: TOperator,
            right: TOperator
          ): Stitch[TOperator] = {
    Stitch.create(
      left,
      right,
      this.getClass
    )
  }
}

private[helpers] object Stitch {
  def create[TOperator <: RelNode](
                                    left: TOperator,
                                    right: TOperator,
                                    c: Class[_ <: Stitch[TOperator]]
                                  ): Stitch[TOperator] = {
    Construct.create(c, left, right)
  }
}
