/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.store.rle

import ch.epfl.dias.cs422.helpers.rel.RelOperator.RLEColumn
import ch.epfl.dias.cs422.helpers.rel.RelOperatorUtilLog
import ch.epfl.dias.cs422.helpers.store.Store

class RLEStandaloneColumnStore private[store](
    private val data: RLEColumn,
    private val rowCount: Long,
    private val columnIndex: Int
) extends Store {
  def getRLEColumn: RLEColumn = {
    if (rowCount == 0) return IndexedSeq()
    val tmp = data
    RelOperatorUtilLog.accesses = RelOperatorUtilLog.accesses + tmp.size
    tmp
  }

  override def getRowCount: Long =
    rowCount // otherwise we can't retrieve it for 0-column tables

  def getColumnIndex: Int = columnIndex
}
