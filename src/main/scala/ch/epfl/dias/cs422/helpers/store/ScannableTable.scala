/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.store

import ch.epfl.dias.cs422.helpers.rel.RelOperator
import ch.epfl.dias.cs422.helpers.rel.RelOperator.{RLEentry, toHomogeneousColumn}
import ch.epfl.dias.cs422.helpers.rel.late.RelOperatorLateUtil.VID
import ch.epfl.dias.cs422.helpers.store.late.LateColumnStore
import ch.epfl.dias.cs422.helpers.store.rle.RLEStore
import ch.epfl.dias.cs422.helpers.{PathUtils, Schema, SqlPrepare}
import com.google.common.collect.ImmutableList
import org.apache.calcite.DataContext
import org.apache.calcite.linq4j.Enumerable
import org.apache.calcite.rel.`type`.{RelDataType, RelDataTypeFactory}
import org.apache.calcite.schema.impl.AbstractTable
import org.apache.calcite.schema.{Statistic, Statistics}
import org.apache.calcite.sql.`type`.SqlTypeName
import org.slf4j.LoggerFactory

import java.io.FileNotFoundException
import java.nio.file.{NoSuchFileException, Path, Paths}
import java.util
import scala.annotation.tailrec
import scala.io.Source
import scala.jdk.CollectionConverters._

class ScannableTable(
    val schema: List[(String, _ >: SqlTypeName with (SqlTypeName, Boolean))],
    val name: String
) extends AbstractTable
    with org.apache.calcite.schema.ScannableTable {
  private val path: Path =
    try {
      if (Schema.schema.contains(this.name)) {
        Paths.get("input", this.name + ".tbl")
      } else {
        Paths.get(this.name)
      }
    } catch {
      case _: NoSuchFileException => null
    }

  override def getRowType(typeFactory: RelDataTypeFactory): RelDataType = {
    typeFactory.createStructType(
      schema
        .map(e =>
          new util.Map.Entry[String, RelDataType] {
            override def getKey: String = e._1

            override def getValue: RelDataType = {
              typeFactory.createTypeWithNullability(
                typeFactory.createSqlType(e._2 match {
                  case (t: SqlTypeName, true) => t
                  case t: SqlTypeName         => t
                }),
                e._2 match {
                  case (_, true) => true
                  case _         => false
                }
              )
            }

            override def setValue(v: RelDataType): RelDataType = null
          }
        )
        .asJava
    )
  }

  private lazy val stat: Statistic = {
    try {
      val f =
        Source.fromInputStream(PathUtils.getResourceAsStream(path.toString))
      val s = Statistics.of(f.getLines.size * 1.0, ImmutableList.of())
      f.close()
      s
    } catch {
      case _: FileNotFoundException => Statistics.UNKNOWN
    }
  }

  override def getStatistic: Statistic = stat

  def getRowCount: Long = stat.getRowCount.toLong

  private lazy val rowStore = new RowStore(readAll().toIndexedSeq)
  private lazy val columnStore =
    new ColumnStore(readAll().toIndexedSeq.transpose.map(toHomogeneousColumn), getRowCount)
  private lazy val paxStore = new PAXStore(
    readAll().toIndexedSeq
      .sliding(RelOperator.blockSize, RelOperator.blockSize)
      .toIndexedSeq
      .map(b => b.transpose),
    getRowCount
  )
  private lazy val lateColumnStore =
    new LateColumnStore(readAll().toIndexedSeq.transpose, getRowCount)

  private lazy val rleStore =
    new RLEStore(
      readAll().toIndexedSeq.transpose.map(c => {
        def rle[X](xs: List[X]): List[RLEentry] = {
          @tailrec
          def rle2(
              done: List[RLEentry],
              curr: X,
              length: Long,
              xs: List[X],
              vid: VID
          ): List[RLEentry] =
            xs match {
              case Nil =>
                done ++ List(RLEentry(vid - length, length, IndexedSeq(curr)))
              case y :: ys =>
                y match {
                  case `curr` => rle2(done, curr, length + 1, ys, vid + 1)
                  case _ =>
                    rle2(
                      done ++ List(
                        RLEentry(vid - length, length, IndexedSeq(curr))
                      ),
                      y,
                      1,
                      ys,
                      vid + 1
                    )
                }
            }
          rle2(Nil, xs.head, 1, xs.tail, 1)
        }
        val ret = rle(c.toList).toIndexedSeq

        val logger = LoggerFactory.getLogger(classOf[ScannableTable])
        logger.debug(ret.toString())

        ret
      }),
      getRowCount
    )

  private[helpers] def getAsRowStore: RowStore = rowStore

  private[helpers] def getAsColumnStore: ColumnStore = columnStore

  private[helpers] def getAsRLEStore: RLEStore = rleStore

  private[helpers] def getAsLateColumnStore : LateColumnStore = lateColumnStore

  private[helpers] def getAsPAXStore: PAXStore = paxStore

  private def readAll(): List[IndexedSeq[Any]] = {
    val reader = new InputTable(
      getRowType(SqlPrepare.cluster.getTypeFactory),
      path,
      SqlPrepare.cluster
    )
    val tmp = reader.readAll()
    reader.close()
    tmp
  }

  override def scan(root: DataContext): Enumerable[Array[AnyRef]] = {
    new InputTable(
      getRowType(SqlPrepare.cluster.getTypeFactory),
      path,
      SqlPrepare.cluster
    ).scan(root)
  }
}
