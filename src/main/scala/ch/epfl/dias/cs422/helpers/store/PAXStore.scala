/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.store

import ch.epfl.dias.cs422.helpers.rel.RelOperator.PAXPage
import ch.epfl.dias.cs422.helpers.rel.RelOperatorUtilLog

class PAXStore private[store] (
    private val data: IndexedSeq[PAXPage],
    private val rowCount: Long
) extends Store {
  def getPAXPage(i: Integer): PAXPage = {
    val tmp = data(i)
    if (tmp.nonEmpty) {
      RelOperatorUtilLog.accesses =
        RelOperatorUtilLog.accesses + tmp.size * tmp.head.size
    }
    tmp
  }

  override def getRowCount: Long = rowCount
}
