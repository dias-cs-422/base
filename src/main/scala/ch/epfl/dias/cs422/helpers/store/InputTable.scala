/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.store

import au.com.bytecode.opencsv.CSVReader
import ch.epfl.dias.cs422.helpers.PathUtils
import ch.epfl.dias.cs422.helpers.rel.RelOperatorUtil
import org.apache.calcite.DataContext
import org.apache.calcite.jdbc.JavaTypeFactoryImpl
import org.apache.calcite.linq4j.Enumerable
import org.apache.calcite.linq4j.tree.Primitive
import org.apache.calcite.plan.RelOptCluster
import org.apache.calcite.rel.`type`.{RelDataType, RelDataTypeFactory}
import org.apache.calcite.rex.RexInputRef
import org.apache.calcite.schema.impl.AbstractTable
import org.apache.calcite.sql.`type`.SqlTypeName

import java.io.InputStreamReader
import java.nio.file.Path
import scala.jdk.CollectionConverters._

class InputTable(
    val rowType: RelDataType,
    val path: Path,
    val cluster: RelOptCluster
) extends AbstractTable
    with org.apache.calcite.schema.ScannableTable {
  private val reader: CSVReader = new CSVReader(
    new InputStreamReader(PathUtils.getResourceAsStream(path.toString)),
    '|'
  )

  private val types = rowType.getFieldList.asScala.toIndexedSeq
    .map(t =>
      cluster.getTypeFactory.createTypeWithNullability(t.getType, false)
    )
    .zipWithIndex
  private val evaluators = RelOperatorUtil.eval(
    cluster,
    types.map { e =>
      cluster.getRexBuilder.makeCast(
        e._1,
        new RexInputRef(
          e._2 /*.getIndex*/,
          cluster.getTypeFactory.createSqlType(SqlTypeName.VARCHAR)
        )
      )
    },
    cluster.getTypeFactory.createStructType(
      types
        .map(_ => cluster.getTypeFactory.createSqlType(SqlTypeName.VARCHAR))
        .asJava,
      rowType.getFieldNames
    ) /*rowType*/
  )

  private def parseLine(values: Array[String]) = {
    val ts = types
    if (values.length == types.length + 1)
      assert(
        values.length == types.length || (values.length == types.length + 1 && values(
          values.length - 1
        ) == "")
      )
    val inp = ts
      .zip(values)
      .map(e =>
        try {
          e._2
        } catch {
          case _: Throwable => e._2
        }
      )

    val jtype = new JavaTypeFactoryImpl(
      cluster.getTypeFactory.getTypeSystem
    )

    val v = evaluators(inp)
      .zip(ts)
      .map({
        case (v, t) =>
          val sType = jtype.getJavaClass(t._1)
          if (Primitive.is(sType)) {
            val v2: AnyVal =
              Primitive.of(sType) match {
                case Primitive.BOOLEAN => Boolean.unbox(v.asInstanceOf[AnyRef])
                case Primitive.INT     => scala.runtime.BoxesRunTime.unboxToInt(v)
                case Primitive.LONG    => Long.unbox(v.asInstanceOf[AnyRef])
                case Primitive.FLOAT   => Float.unbox(v.asInstanceOf[AnyRef])
                case Primitive.DOUBLE  => Double.unbox(v.asInstanceOf[AnyRef])
                case Primitive.SHORT   => Short.unbox(v.asInstanceOf[AnyRef])
                case Primitive.BYTE    => Byte.unbox(v.asInstanceOf[AnyRef])
                case Primitive.CHAR    => Char.unbox(v.asInstanceOf[AnyRef])
              }
            v2
          } else {
            v
          }
      })
    v
  }

  def readNext(): IndexedSeq[Any] = {
    val values = reader.readNext()
    if (values == null) return null
    parseLine(values)
  }

  def readAll(): List[IndexedSeq[Any]] =
    reader.readAll().asScala.toList.map(parseLine)

  def close(): Unit = {
    reader.close()
  }

  override def scan(root: DataContext): Enumerable[Array[AnyRef]] = {
    throw new RuntimeException("Unreachable code reached")
  }

  override def getRowType(typeFactory: RelDataTypeFactory): RelDataType =
    rowType
}
