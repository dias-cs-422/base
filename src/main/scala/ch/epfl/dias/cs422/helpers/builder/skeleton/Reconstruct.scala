package ch.epfl.dias.cs422.helpers.builder.skeleton

import ch.epfl.dias.cs422.helpers.Construct
import org.apache.calcite.plan.RelTraitSet
import org.apache.calcite.rel.RelNode

import java.util

/**
  * @inheritdoc
  *
  * @see [[ch.epfl.dias.cs422.helpers.builder.skeleton.logical.LogicalReconstruct]]
  */
class Reconstruct[TOperator <: RelNode] protected (
    left: TOperator,
    right: TOperator
) extends logical.LogicalReconstruct(left, right) {

  override def copy(
      traitSet: RelTraitSet,
      inputs: util.List[RelNode]
  ): Reconstruct[TOperator] = {
    assert(inputs.size() == 2)
    copy(
      inputs.get(0).asInstanceOf[TOperator],
      inputs.get(1).asInstanceOf[TOperator]
    )
  }

  def copy(
      left: TOperator,
      right: TOperator
  ): Reconstruct[TOperator] = {
    Reconstruct.create(
      left,
      right,
      this.getClass
    )
  }
}

private[helpers] object Reconstruct {
  def create[TOperator <: RelNode](
      left: TOperator,
      right: TOperator,
      c: Class[_ <: Reconstruct[TOperator]]
  ): Reconstruct[TOperator] = {
    Construct.create(c, left, right)
  }
}
