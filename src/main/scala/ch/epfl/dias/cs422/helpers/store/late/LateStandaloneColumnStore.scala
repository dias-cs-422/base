package ch.epfl.dias.cs422.helpers.store.late

import ch.epfl.dias.cs422.helpers.rel.RelOperator.{Column, Elem}
import ch.epfl.dias.cs422.helpers.rel.RelOperatorUtilLog
import ch.epfl.dias.cs422.helpers.rel.late.RelOperatorLateUtil.VID
import ch.epfl.dias.cs422.helpers.store.Store

class LateStandaloneColumnStore private[store](
                                                private val data: Column,
                                                private val rowCount: Long,
                                                private val columnIndex: Int
                                              ) extends Store {
  /**
    *
    * @return The Column object. It contains each row's element in position "vid"
    */
  def getColumn: Column = {
    if (rowCount == 0) return IndexedSeq()
    val tmp = data
    RelOperatorUtilLog.accesses = RelOperatorUtilLog.accesses + rowCount
    tmp
  }

  /**
    *
    * @param vid for accessed row
    * @return The column's element for the requested vid or None if out of range
    */
  def getElement(vid : VID) : Option[Elem] = {
    if (rowCount == 0 || vid >= rowCount) return None
    val elem = data(vid.toInt)
    RelOperatorUtilLog.accesses = RelOperatorUtilLog.accesses + 1
    Option(elem)
  }

  override def getRowCount: Long =
    rowCount // otherwise we can't retrieve it for 0-column tables

  def getColumnIndex: Int = columnIndex

}
