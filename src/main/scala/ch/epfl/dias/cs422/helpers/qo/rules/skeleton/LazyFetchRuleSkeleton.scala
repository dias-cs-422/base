package ch.epfl.dias.cs422.helpers.qo.rules.skeleton

import ch.epfl.dias.cs422.helpers.builder.skeleton.logical.LogicalFetch
import org.apache.calcite.plan.{RelOptRuleCall, RelRule}
import org.apache.calcite.rel.RelNode

/**
  * RelRule (optimization rule) that finds an operator that stitches a new column
  * to the late materialized tuple and transforms stitching into a fetch operator.
  *
  * To use this rule: LazyFetchRule.Config.DEFAULT.toRule()
  *
  * @param config configuration parameters of the optimization rule
  */
class LazyFetchRuleSkeleton protected(config: RelRule.Config)
  extends RelRule(
    config
  ) {
  def onMatchHelper(call: RelOptRuleCall): RelNode = ???

  override final def onMatch(call: RelOptRuleCall): Unit = {
    val result = onMatchHelper(call)

    assert(result.isInstanceOf[LogicalFetch])
    val fetch = result.asInstanceOf[LogicalFetch]
    assert(fetch.getProjects.isEmpty)

    call.transformTo(
      result
    )
  }
}