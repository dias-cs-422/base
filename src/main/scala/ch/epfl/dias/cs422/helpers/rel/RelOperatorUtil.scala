/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.rel

import ch.epfl.dias.cs422.helpers.rel.RelOperator._
import ch.epfl.dias.cs422.helpers.rex.{RexExecutor, TupleContext}
import org.apache.calcite.plan.RelOptCluster
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rex.RexNode

import scala.jdk.CollectionConverters._

private[helpers] object RelOperatorUtilLog {
  var accesses: Long = 0

  // FIXME: restrict access
  def resetFieldAccessCount(): Unit = {
    accesses = 0
  }

  def getFieldAccessCount: Long = accesses
}

private[helpers] object RelOperatorUtil {
  protected[helpers] def eval(
      cluster: RelOptCluster,
      e: RexNode,
      inputRowType: RelDataType
  ): Tuple => Elem = {
    val f = eval(cluster, IndexedSeq(e), inputRowType)
    tuple => f(tuple)(0)
  }

  protected[helpers] def eval(
      cluster: RelOptCluster,
      e: IndexedSeq[RexNode],
      inputRowType: RelDataType
  ): Tuple => Tuple = {
    val f = RexExecutor.getExecutable(
      cluster.getRexBuilder,
      e.asJava,
      inputRowType
    )
    val fun = f.getFunction

    def t(tuple: Tuple): Tuple = {
      fun
        .apply(new TupleContext {
          override def field(index: Int): Any = {
            assert(
              tuple != null,
              "You asked to evaluate an expression on a Nil tuple"
            )
            if (tuple.size != inputRowType.getFieldCount)
              println(
                tuple + " " + tuple.size + " " + inputRowType + " " + inputRowType.getFieldCount
              )
            assert(
              tuple.size == inputRowType.getFieldCount,
              "You provided a tuple that does not match the specifidied inputRowType"
            )
//          println("Accessing field: " + index)
            tuple(index)
          }
        })
        .toIndexedSeq
        .asInstanceOf[Tuple]
    }

    t
  }

  protected[helpers] def map(
      cluster: RelOptCluster,
      e: RexNode,
      inputRowType: RelDataType,
      isFilterCondition: Boolean
  ): IndexedSeq[HomogeneousColumn] => HomogeneousColumn = {
    val f = RexExecutor.getMapExecutable(
      cluster.getRexBuilder,
      e,
      inputRowType,
      isFilterCondition
    )
    val fun = f

    def t(cols: IndexedSeq[HomogeneousColumn]): HomogeneousColumn = {
      fun
        .apply(new TupleContext {
          override def field(index: Int): Any = {
            if (cols.size != inputRowType.getFieldCount)
              println(
                cols.size + " " + inputRowType + " " + inputRowType.getFieldCount
              )
            assert(
              cols.size == inputRowType.getFieldCount,
              "You provided a tuple that does not match the specifidied inputRowType"
            )
            //          println("Accessing field: " + index)
            cols(index)
          }

          override def field2(index: Int): HomogeneousColumn = {
            if (cols.size != inputRowType.getFieldCount)
              println(
                cols.size + " " + inputRowType + " " + inputRowType.getFieldCount
              )
            assert(
              cols.size == inputRowType.getFieldCount,
              "You provided a tuple that does not match the specifidied inputRowType"
            )
            //          println("Accessing field: " + index)
            cols(index)
          }

          override def size(): Int = { cols.head.size }
        })
    }

    t
  }
}
