package ch.epfl.dias.cs422.helpers.builder.skeleton

import ch.epfl.dias.cs422.helpers.Construct
import ch.epfl.dias.cs422.helpers.store.late.LateStandaloneColumnStore
import org.apache.calcite.plan.RelTraitSet
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rel.{RelNode, core}
import org.apache.calcite.rex.RexNode

import java.util

class Fetch[TOperator <: RelNode] protected (input: TOperator, fetchType: RelDataType, column: LateStandaloneColumnStore, projects: Option[java.util.List[_ <: RexNode]]) extends logical.LogicalFetch(input, fetchType, column, projects) {
  override def copy(
                     traitSet: RelTraitSet,
                     inputs: util.List[RelNode]
                   ): Fetch[TOperator] = {
    assert(inputs.size() == 1)
    copy(
      inputs.get(0).asInstanceOf[TOperator]
    )
  }

  override def copy(input: RelNode): Fetch[TOperator] = Fetch.create(input.asInstanceOf[TOperator], fetchType, column, projects, this.getClass)
}


private[helpers] object Fetch {
  def create[TOperator <: RelNode](
                                    input: TOperator,
                                    fetchType: RelDataType,
                                    column: LateStandaloneColumnStore,
                                    projects: Option[java.util.List[_ <: RexNode]],
                                    c: Class[_ <: Fetch[TOperator]]
                                  ): Fetch[TOperator] = {
    Construct.create(c, input, fetchType, column, projects)
  }
}
