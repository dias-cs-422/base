package ch.epfl.dias.cs422.helpers.builder.skeleton.logical

import ch.epfl.dias.cs422.helpers.Construct
import ch.epfl.dias.cs422.helpers.store.late.LateStandaloneColumnStore
import org.apache.calcite.plan.RelTraitSet
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rel.metadata.RelMetadataQuery
import org.apache.calcite.rel.{RelNode, SingleRel}
import org.apache.calcite.rex.{RexNode, RexUtil}
import org.apache.calcite.sql.validate.SqlValidatorUtil

import java.util
import java.util.Collections

class LogicalFetch (
                              input: RelNode,
                              fetchType: RelDataType,
                              column: LateStandaloneColumnStore,
                              projects: Option[java.util.List[_ <: RexNode]]
                            ) extends SingleRel(input.getCluster, input.getTraitSet, input){

  def getFetchType : RelDataType = {
    fetchType
  }

  def getColumn : LateStandaloneColumnStore = {
    column
  }

  def getProjects : Option[java.util.List[_ <: RexNode]] = {
    projects
  }

  final override def deriveRowType: RelDataType = {
   val rowType =
     if (projects != None) {
       RexUtil.createStructType(getCluster.getTypeFactory, projects.get, null, null)
     } else {
       fetchType
     }

    SqlValidatorUtil.createJoinType(
      getCluster.getTypeFactory,
      input.getRowType,
      rowType,
      null,
      Collections.emptyList
    )
  }

  final override def estimateRowCount(mq: RelMetadataQuery): Double = {
    mq.getRowCount(input)
  }

  override def copy(
                     traitSet: RelTraitSet,
                     inputs: util.List[RelNode]
                   ): LogicalFetch = {
    assert(inputs.size == 1)
    copy(
      inputs.get(0)
    )
  }

  def copy(
            input: RelNode
          ): LogicalFetch = {
    LogicalFetch.create(
      input,
      fetchType,
      column,
      projects,
      this.getClass
    )
  }
}

object LogicalFetch {
  def create(
              input: RelNode,
              fetchType: RelDataType,
              column: LateStandaloneColumnStore,
              projects: Option[java.util.List[_ <: RexNode]],
              c: Class[_ <: LogicalFetch]
            ): LogicalFetch = {
    Construct.create(c, input, fetchType, column, projects)
  }
}
