package ch.epfl.dias.cs422.helpers.qo.rules

import ch.epfl.dias.cs422.helpers.builder.skeleton.logical.LogicalDrop
import org.apache.calcite.plan.RelOptRule.{any, operand}
import org.apache.calcite.plan.{RelOptRule, RelOptRuleCall}
import org.apache.calcite.rel.core.Project

import java.util

class ProjectDropTransposeRule
  extends RelOptRule(
    // Match Project(logical.LogicalDrop(any...))
    operand(classOf[Project], operand(classOf[LogicalDrop], any)),
    "ProjectDropTransposeRule"
  ) {

  override def onMatch(call: RelOptRuleCall): Unit = {
    val proj: Project = call.rel(0)
    val dec: LogicalDrop = call.rel(1)
    call.transformTo(
      LogicalDrop.create(
        proj.copy(
          proj.getTraitSet,
          util.Collections.singletonList(dec.getInput)
        ),
        classOf[LogicalDrop]
      )
    )
  }
}
