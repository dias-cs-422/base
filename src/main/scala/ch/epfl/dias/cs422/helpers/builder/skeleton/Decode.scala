package ch.epfl.dias.cs422.helpers.builder.skeleton

import ch.epfl.dias.cs422.helpers.Construct
import org.apache.calcite.plan.RelTraitSet
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rel.metadata.RelMetadataQuery
import org.apache.calcite.rel.{RelNode, SingleRel}

import java.util

/** Decodes RLE tuples to normal tuples.
  *
  * For example, if the input operator produces: RLEentry(4, 2, Tuple("a", 5)), RLEentry(8, 3, Tuple("b", 6)), then
  * the Decode operator should produce: Tuple("a", 5), Tuple("a", 5), Tuple("b", 6), Tuple("b", 6), Tuple("b", 6)
  *
  * @param input Operator that produces RLE entries
  */
class Decode[TOperator <: RelNode, TInputOperator <: RelNode] protected (
    input: TInputOperator
) extends logical.LogicalDecode(input) {


  override def copy(
      traitSet: RelTraitSet,
      inputs: util.List[RelNode]
  ): Decode[TOperator, TInputOperator] = {
    assert(inputs.size == 1)
    copy(
      inputs.get(0).asInstanceOf[TInputOperator]
    )
  }

  def copy(
      input: TInputOperator
  ): Decode[TOperator, TInputOperator] = {
    Decode.create(
      input,
      this.getClass
    )
  }
}

private[helpers] object Decode {
  def create[TOperator <: RelNode, TInputOperator <: RelNode](
      input: TInputOperator,
      c: Class[_ <: Decode[TOperator, TInputOperator]]
  ): Decode[TOperator, TInputOperator] = {
    Construct.create(c, input)
  }
}
