/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.builder

import ch.epfl.dias.cs422.helpers.builder.skeleton._
import ch.epfl.dias.cs422.helpers.builder.skeleton.logical.{LogicalDecode, LogicalDrop, LogicalFetch, LogicalReconstruct, LogicalStitch}
import ch.epfl.dias.cs422.helpers.rel.RelOperator
import ch.epfl.dias.cs422.helpers.store.late.LateStandaloneColumnStore
import ch.epfl.dias.cs422.helpers.store.{ScannableTable, Store}
import ch.epfl.dias.cs422.helpers.{PrintUtil, rex, store}
import com.google.common.collect.ImmutableList
import org.apache.calcite.plan.RelOptTable.ToRelContext
import org.apache.calcite.plan.{RelOptTable, ViewExpanders}
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rel.core.{Aggregate => _, Filter => _, Join => _, Project => _, Sort => _, _}
import org.apache.calcite.rel.hint.RelHint
import org.apache.calcite.rel.logical._
import org.apache.calcite.rel.{RelCollation, RelNode, RelShuttle, RelShuttleImpl}
import org.apache.calcite.rex.{RexLiteral, RexNode, RexUtil}
import org.apache.calcite.sql.validate.SqlValidatorUtil
import org.apache.calcite.util.ImmutableBitSet

import java.util
import scala.jdk.CollectionConverters._

object Factories {
  val VOLCANO_INSTANCE = new Factories[
    ch.epfl.dias.cs422.helpers.rel.early.volcano.Operator
  ]("ch.epfl.dias.cs422.rel.early.volcano")
  val BLOCKATATIME_INSTANCE = new Factories[
    ch.epfl.dias.cs422.helpers.rel.early.blockatatime.Operator
  ]("ch.epfl.dias.cs422.rel.early.blockatatime")
  val OPERATOR_AT_A_TIME_INSTANCE = new Factories[
    ch.epfl.dias.cs422.helpers.rel.early.operatoratatime.Operator
  ]("ch.epfl.dias.cs422.rel.early.operatoratatime")
  val LAZY_OPERATOR_AT_A_TIME_INSTANCE = new Factories[
    ch.epfl.dias.cs422.helpers.rel.late.operatoratatime.Operator
  ]("ch.epfl.dias.cs422.rel.late.operatoratatime")
  val VOLCANO_RLE_INSTANCE = new RLEFactories[
    ch.epfl.dias.cs422.helpers.rel.early.volcano.Operator
  ]("ch.epfl.dias.cs422.rel.early.volcano")
  val VOLCANO_LATE_INSTANCE = new LateFactories[
    ch.epfl.dias.cs422.helpers.rel.early.volcano.Operator
  ]("ch.epfl.dias.cs422.rel.early.volcano")
  val COLUMN_AT_A_TIME_INSTANCE = new Factories[
    ch.epfl.dias.cs422.helpers.rel.early.columnatatime.Operator
  ]("ch.epfl.dias.cs422.rel.early.columnatatime")
}

private[helpers] class Factories[
    TOperator <: RelNode with RelOperator
] protected (private val packagePath: String) {
  protected type TFilter = TOperator with Filter[TOperator]
  protected type TScan = TOperator with Scan[TOperator]
  protected type TProject = TOperator with Project[TOperator]
  protected type TAggregate = TOperator with Aggregate[TOperator]
  protected type TSort = TOperator with Sort[TOperator]
  protected type TJoin = TOperator with Join[TOperator]
  protected type OpType = TOperator

  protected lazy val filter: Class[TFilter] =
    Class.forName(packagePath + "." + "Filter").asInstanceOf[Class[TFilter]]
  protected lazy val scan: Class[TScan] =
    Class.forName(packagePath + "." + "Scan").asInstanceOf[Class[TScan]]
  protected lazy val project: Class[TProject] =
    Class.forName(packagePath + "." + "Project").asInstanceOf[Class[TProject]]
  protected lazy val join: Class[TJoin] =
    Class.forName(packagePath + "." + "Join").asInstanceOf[Class[TJoin]]
  protected lazy val sort: Class[TSort] =
    Class.forName(packagePath + "." + "Sort").asInstanceOf[Class[TSort]]
  protected lazy val aggregate: Class[TAggregate] = Class
    .forName(packagePath + "." + "Aggregate")
    .asInstanceOf[Class[TAggregate]]

  private[helpers] val FILTER_FACTORY: RelFactories.FilterFactory =
    (input: RelNode, condition: RexNode, _: util.Set[CorrelationId]) =>
      Filter.create(input.asInstanceOf[TOperator], condition, filter)

  protected class ScanFactory extends RelFactories.TableScanFactory {
    override def createScan(
        toRelContext: RelOptTable.ToRelContext,
        table: RelOptTable
    ): RelNode =
      createScan(toRelContext, table, (s: ScannableTable) => s.getAsRowStore)

    def createScan(
        toRelContext: RelOptTable.ToRelContext,
        table: RelOptTable,
        tableToScan: ScannableTable => Store
    ): RelNode = Scan.create(toRelContext.getCluster, table, scan, tableToScan)
  }

  private[helpers] val SCAN_FACTORY: ScanFactory = new ScanFactory

  private[helpers] val PROJECT_FACTORY: RelFactories.ProjectFactory = (
      input: RelNode,
      _: util.List[RelHint],
      childExprs: util.List[_ <: RexNode],
      fieldNames: util.List[String]
  ) => {
    val rowType = RexUtil.createStructType(
      input.getCluster.getTypeFactory,
      childExprs,
      fieldNames,
      SqlValidatorUtil.F_SUGGESTER
    )
    Project.create(input.asInstanceOf[TOperator], childExprs, rowType, project)
  }

  private[helpers] val JOIN_FACTORY: RelFactories.JoinFactory = (
      left: RelNode,
      right: RelNode,
      _: util.List[RelHint],
      condition: RexNode,
      variablesSet: util.Set[CorrelationId],
      joinType: JoinRelType,
      _: Boolean
  ) => {
    assert(joinType == JoinRelType.INNER)
    assert(variablesSet.size() == 0)
    Join.create(
      left.asInstanceOf[TOperator],
      right.asInstanceOf[TOperator],
      condition,
      join
    )
  }

  private[helpers] val SORT_FACTORY: RelFactories.SortFactory = (
      input: RelNode,
      collation: RelCollation,
      offset: RexNode,
      fetch: RexNode
  ) => {
    Sort.create(
      input.asInstanceOf[TOperator],
      collation,
      if (offset == null) Option.empty[Int]
      else
        Option[Int](
          offset
            .asInstanceOf[RexLiteral]
            .getValueAs(classOf[Integer])
            .asInstanceOf[Int]
        ),
      if (fetch == null) Option.empty[Int]
      else
        Option[Int](
          fetch
            .asInstanceOf[RexLiteral]
            .getValueAs(classOf[Integer])
            .asInstanceOf[Int]
        ),
      sort
    )
  }
  private[helpers] val AGGREGATE_FACTORY: RelFactories.AggregateFactory = (
      input: RelNode,
      _: util.List[RelHint],
      groupSet: ImmutableBitSet,
      groupSets: ImmutableList[ImmutableBitSet],
      aggCalls: util.List[AggregateCall]
  ) => {
    assert(
      groupSets == null || (groupSets.size() == 1 && groupSets
        .get(0) == groupSet)
    )
    Aggregate.create(
      input.asInstanceOf[TOperator],
      groupSet,
      aggCalls.asScala.toIndexedSeq.map(a => rex.AggregateCall(a)),
      aggregate
    )
  }

  private[helpers] def preprocessPlan(relNode: RelNode): RelNode = relNode

  protected class Implementor(val storeGenerator: ScannableTable => Store)
      extends RelShuttle {
    def unsupported(): RelNode = {
      throw new RuntimeException("Unreachable code reached")
    }

    override def visit(calc: LogicalCalc): RelNode = unsupported()

    override def visit(scan: TableScan): RelNode = {
      scan match {
        case s: store.rle.rel.early.volcano.RLEColumnScan =>
          store.rle.rel.early.volcano.RLEColumnScan
            .create(
              scan.getCluster,
              scan.getTable,
              s.tableToStore,
              s.colName
            )
        case s: store.late.rel.late.volcano.LateColumnScan =>
          store.late.rel.late.volcano.LateColumnScan
            .create(
              scan.getCluster,
              scan.getTable,
              s.tableToStore,
              s.colName
            )
        case _ =>
          SCAN_FACTORY
            .createScan(
              ViewExpanders.simpleContext(scan.getCluster),
              scan.getTable,
              storeGenerator
            )
      }
    }

    override def visit(scan: TableFunctionScan): RelNode = unsupported()

    override def visit(values: LogicalValues): RelNode = unsupported()

    override def visit(filter: LogicalFilter): RelNode =
      FILTER_FACTORY.createFilter(
        filter.getInput.accept(this),
        filter.getCondition,
        filter.getVariablesSet
      )

    override def visit(project: LogicalProject): RelNode =
      PROJECT_FACTORY.createProject(
        project.getInput.accept(this),
        project.getHints,
        project.getProjects,
        project.getNamedProjects.asScala.map(e => e.right).asJava
      )

    override def visit(join: LogicalJoin): RelNode = {
      JOIN_FACTORY.createJoin(
        join.getLeft.accept(this),
        join.getRight.accept(this),
        join.getHints,
        join.getCondition,
        join.getVariablesSet,
        join.getJoinType,
        join.isSemiJoinDone
      )
    }

    override def visit(correlate: LogicalCorrelate): RelNode = {
      visit(correlate.asInstanceOf[RelNode])
    }

    override def visit(union: LogicalUnion): RelNode = unsupported()

    override def visit(intersect: LogicalIntersect): RelNode = unsupported()

    override def visit(minus: LogicalMinus): RelNode = unsupported()

    override def visit(aggregate: LogicalAggregate): RelNode =
      AGGREGATE_FACTORY.createAggregate(
        aggregate.getInput.accept(this),
        aggregate.getHints,
        aggregate.getGroupSet,
        aggregate.getGroupSets,
        aggregate.getAggCallList
      )

    override def visit(m: LogicalMatch): RelNode = unsupported()

    override def visit(sort: LogicalSort): RelNode =
      SORT_FACTORY.createSort(
        sort.getInput.accept(this),
        sort.getCollation,
        sort.offset,
        sort.fetch
      )

    override def visit(exchange: LogicalExchange): RelNode = unsupported()

    override def visit(other: RelNode): RelNode = {
      println("Under conversion tree:")
      PrintUtil.printTree(other)
      throw new IllegalArgumentException("Unexpected RelNode: " + other)
    }

    override def visit(modify: LogicalTableModify): RelNode = unsupported()
  }

  protected def getAsStore(s: String): ScannableTable => Store = {
    s match {
      case "rowstore"    => (s: ScannableTable) => s.getAsRowStore
      case "columnstore" => (s: ScannableTable) => s.getAsColumnStore
      case "latecolumnstore" => (s: ScannableTable) => s.getAsLateColumnStore
      case "paxstore"    => (s: ScannableTable) => s.getAsPAXStore
      case "rlestore"    => (s: ScannableTable) => s.getAsRLEStore
      case _             => (s: ScannableTable) => s.getAsRowStore
    }
  }

  private[helpers] def implementPlan(
      rel: RelNode,
      storeType: String
  ): RelNode = {
    rel.accept(new Implementor(getAsStore(storeType)))
  }
}

private[helpers] class RLEFactories[
    TOperator <: RelNode with RelOperator
](private val packagePath: String)
    extends Factories[TOperator](packagePath) {
  protected type TRLEOperator =
    ch.epfl.dias.cs422.helpers.rel.early.volcano.rle.Operator with RelOperator

  protected type TDecode = TOperator with Decode[TOperator, TRLEOperator]
  protected type TReconstruct = TRLEOperator with Reconstruct[TRLEOperator]
  protected type TRLEScan = TRLEOperator with Scan[TRLEOperator]
  protected type TRLEFilter = TRLEOperator with Filter[TRLEOperator]
  protected type TRLEProject = TRLEOperator with Project[TRLEOperator]
  protected type TRLEAggregate = TRLEOperator with Aggregate[TRLEOperator]
  protected type TRLEJoin = TRLEOperator with Join[TRLEOperator]

  private lazy val rlescan =
    classOf[
      ch.epfl.dias.cs422.helpers.store.rle.rel.early.volcano.RLEColumnScan
    ]
  private lazy val rlereco =
    Class
      .forName(packagePath + "." + "rle." + "Reconstruct")
      .asInstanceOf[Class[TReconstruct]]
  private lazy val rledeco =
    Class
      .forName(packagePath + "." + "rle." + "Decode")
      .asInstanceOf[Class[TDecode]]
  private lazy val rleproj =
    Class
      .forName(packagePath + "." + "rle.RLE" + "Project")
      .asInstanceOf[Class[TRLEProject]]
  private lazy val rlefilt =
    Class
      .forName(packagePath + "." + "rle.RLE" + "Filter")
      .asInstanceOf[Class[TRLEFilter]]
  private lazy val rleagg =
    Class
      .forName(packagePath + "." + "rle.RLE" + "Aggregate")
      .asInstanceOf[Class[TRLEAggregate]]
  private lazy val rlejoin =
    Class
      .forName(packagePath + "." + "rle.RLE" + "Join")
      .asInstanceOf[Class[TRLEJoin]]

  private[helpers] class RLEScanFactory extends ScanFactory {
    override def createScan(
        toRelContext: ToRelContext,
        table: RelOptTable,
        tableToScan: ScannableTable => Store
    ): RelNode = {
      Scan.create(
        toRelContext.getCluster,
        table,
        rlescan,
        tableToScan
      )
    }
  }

  private[helpers] class RLEDecodeFactory {
    def createDecode(
        input: RelNode
    ): RelNode = {
      logical.LogicalDecode.create(
        input.asInstanceOf[TOperator],
        rledeco
      )
    }
  }

  private[helpers] class RLEReconstructFactory {
    def createReconstruct(
        left: RelNode,
        right: RelNode
    ): RelNode = {
      logical.LogicalReconstruct.create(
        left.asInstanceOf[TOperator],
        right.asInstanceOf[TOperator],
        rlereco
      )
    }
  }

  override private[helpers] val SCAN_FACTORY: ScanFactory = new RLEScanFactory

  private[helpers] val DECODE_FACTORY: RLEDecodeFactory =
    new RLEDecodeFactory
  private[helpers] val RECONSTRUCT_FACTORY: RLEReconstructFactory =
    new RLEReconstructFactory

  private[helpers] override val PROJECT_FACTORY: RelFactories.ProjectFactory = (
      input: RelNode,
      _: util.List[RelHint],
      childExprs: util.List[_ <: RexNode],
      fieldNames: util.List[String]
  ) => {
    input match {
      case rleop: TRLEOperator =>
        val rowType = RexUtil.createStructType(
          input.getCluster.getTypeFactory,
          childExprs,
          fieldNames,
          SqlValidatorUtil.F_SUGGESTER
        )
        Project.create(
          rleop,
          childExprs,
          rowType,
          rleproj
        )
      case op: TOperator =>
        val rowType = RexUtil.createStructType(
          input.getCluster.getTypeFactory,
          childExprs,
          fieldNames,
          SqlValidatorUtil.F_SUGGESTER
        )
        Project.create(
          op,
          childExprs,
          rowType,
          project
        )
    }
  }

  private[helpers] override val FILTER_FACTORY: RelFactories.FilterFactory =
    (input: RelNode, condition: RexNode, _: util.Set[CorrelationId]) =>
      input match {
        case rleop: TRLEOperator =>
          Filter.create(rleop, condition, rlefilt)
        case op: TOperator =>
          Filter.create(op, condition, filter)
      }

  private[helpers] override val AGGREGATE_FACTORY
      : RelFactories.AggregateFactory = (
      input: RelNode,
      _: util.List[RelHint],
      groupSet: ImmutableBitSet,
      groupSets: ImmutableList[ImmutableBitSet],
      aggCalls: util.List[AggregateCall]
  ) => {
    assert(
      groupSets == null || (groupSets.size() == 1 && groupSets
        .get(0) == groupSet)
    )
    input match {
      case rleop: TRLEOperator =>
        Aggregate.create(
          rleop,
          groupSet,
          aggCalls.asScala.toIndexedSeq.map(a => rex.AggregateCall(a)),
          rleagg
        )
      case op: TOperator =>
        Aggregate.create(
          op,
          groupSet,
          aggCalls.asScala.toIndexedSeq.map(a => rex.AggregateCall(a)),
          aggregate
        )
    }
  }

  private[helpers] override val JOIN_FACTORY: RelFactories.JoinFactory = (
      left: RelNode,
      right: RelNode,
      _: util.List[RelHint],
      condition: RexNode,
      variablesSet: util.Set[CorrelationId],
      joinType: JoinRelType,
      _: Boolean
  ) => {
    assert(joinType == JoinRelType.INNER)
    assert(variablesSet.size() == 0)
    left match {
      case _: TRLEOperator =>
        Join.create(
          left.asInstanceOf[TRLEOperator],
          right.asInstanceOf[TRLEOperator],
          condition,
          rlejoin
        )
      case _: TOperator =>
        Join.create(
          left.asInstanceOf[TOperator],
          right.asInstanceOf[TOperator],
          condition,
          join
        )
    }
  }

  class RLEImplementor(storeGenerator: ScannableTable => Store)
      extends Implementor(storeGenerator) {
    def visit(decode: LogicalDecode): RelNode = {
      DECODE_FACTORY.createDecode(
        decode.getInput.accept(this)
      )
    }

    def visit(reconstruct: LogicalReconstruct): RelNode = {
      RECONSTRUCT_FACTORY.createReconstruct(
        reconstruct.getLeft.accept(this),
        reconstruct.getRight.accept(this)
      )
    }

    override def visit(other: RelNode): RelNode = {
      other match {
        case decode: LogicalDecode           => visit(decode)
        case reconstruct: LogicalReconstruct => visit(reconstruct)
        case _                               => super.visit(other)
      }
    }
  }

  override private[helpers] def preprocessPlan(rel: RelNode): RelNode = {
    rel.accept(new RelShuttleImpl() {
      override def visit(table: TableScan): RelNode =
        logical.LogicalDecode.create(
          (0 until table.getRowType.getFieldCount)
            .map(i => {
              ch.epfl.dias.cs422.helpers.store.rle.rel.early.volcano.RLEColumnScan
                .create(
                  rel.getCluster,
                  table.getTable,
                  tbl => {
                    val store = tbl.getAsRLEStore
                    store.getStandaloneColumnAsStore(i).asInstanceOf[Store]
                  },
                  table.getRowType.getFieldList.get(i).getName
                )
                .asInstanceOf[RelNode]
            })
            .reduce((a, b) =>
              logical.LogicalReconstruct
                .create(
                  a,
                  b,
                  classOf[
                    ch.epfl.dias.cs422.helpers.builder.skeleton.logical.LogicalReconstruct
                  ]
                )
                .asInstanceOf[RelNode]
            ),
          classOf[
            ch.epfl.dias.cs422.helpers.builder.skeleton.logical.LogicalDecode
          ]
        )

      override def visit(other: RelNode): RelNode = {
        other.copy(
          other.getTraitSet,
          other.getInputs.asScala.map(inp => inp.accept(this)).asJava
        )
      }
    })
  }

  override private[helpers] def implementPlan(
      rel: RelNode,
      storeType: String
  ): RelNode = {
    rel.accept(new RLEImplementor(getAsStore(storeType)))
  }

}

private[helpers] class LateFactories[
  TOperator <: RelNode with RelOperator
](private val packagePath: String)
  extends Factories[TOperator](packagePath) {
  protected type TLateOperator =
    ch.epfl.dias.cs422.helpers.rel.late.volcano.naive.Operator with RelOperator

  protected type TDrop = TOperator with Drop[TOperator, TLateOperator]
  protected type TStitch = TLateOperator with Stitch[TLateOperator]
  protected type TFetch = TLateOperator with Fetch[TLateOperator]
  protected type TLateScan = TLateOperator with Scan[TLateOperator]
  protected type TLateFilter = TLateOperator with Filter[TLateOperator]
  protected type TLateProject = TLateOperator with Project[TLateOperator]
  protected type TLateJoin = TLateOperator with Join[TLateOperator]

  private lazy val latescan =
    classOf[
      ch.epfl.dias.cs422.helpers.store.late.rel.late.volcano.LateColumnScan
    ]
  private lazy val latefetch =
    Class
      .forName(packagePath + "." + "late." + "Fetch")
      .asInstanceOf[Class[TFetch]]
  private lazy val latereco =
    Class
      .forName(packagePath + "." + "late." + "Stitch")
      .asInstanceOf[Class[TStitch]]
  private lazy val latedeco =
    Class
      .forName(packagePath + "." + "late." + "Drop")
      .asInstanceOf[Class[TDrop]]
  private lazy val lateproj =
    Class
      .forName(packagePath + "." + "late.Late" + "Project")
      .asInstanceOf[Class[TLateProject]]
  private lazy val latefilt =
    Class
      .forName(packagePath + "." + "late.Late" + "Filter")
      .asInstanceOf[Class[TLateFilter]]
  private lazy val latejoin =
    Class
      .forName(packagePath + "." + "late.Late" + "Join")
      .asInstanceOf[Class[TLateJoin]]

  private[helpers] class LateScanFactory extends ScanFactory {
    override def createScan(
                             toRelContext: ToRelContext,
                             table: RelOptTable,
                             tableToScan: ScannableTable => Store
                           ): RelNode = {
      Scan.create(
        toRelContext.getCluster,
        table,
        latescan,
        tableToScan
      )
    }
  }

  private[helpers] class LateDropFactory {
    def createDrop(
                      input: RelNode
                    ): RelNode = {
      logical.LogicalDrop.create(
        input.asInstanceOf[TOperator],
        latedeco
      )
    }
  }

  private[helpers] class LateStitchFactory {
    def createStitch(
                           left: RelNode,
                           right: RelNode
                         ): RelNode = {
      logical.LogicalStitch.create(
        left.asInstanceOf[TOperator],
        right.asInstanceOf[TOperator],
        latereco
      )
    }
  }

  private[helpers] class LateFetchFactory {
    def createFetch(
                           input: RelNode,
                           fetchType: RelDataType,
                           column: LateStandaloneColumnStore,
                           projects: Option[java.util.List[_ <: RexNode]]
                         ): RelNode = {
      logical.LogicalFetch.create(
        input.asInstanceOf[TOperator],
        fetchType,
        column,
        projects,
        latefetch
      )
    }
  }

  override private[helpers] val SCAN_FACTORY: ScanFactory = new LateScanFactory

  private[helpers] val DROP_FACTORY: LateDropFactory =
    new LateDropFactory
  private[helpers] val STITCH_FACTORY: LateStitchFactory =
    new LateStitchFactory
  private[helpers] val FETCH_FACTORY: LateFetchFactory =
    new LateFetchFactory

  private[helpers] override val PROJECT_FACTORY: RelFactories.ProjectFactory = (
                                                                                 input: RelNode,
                                                                                 _: util.List[RelHint],
                                                                                 childExprs: util.List[_ <: RexNode],
                                                                                 fieldNames: util.List[String]
                                                                               ) => {
    input match {
      case lateop: TLateOperator =>
        val rowType = RexUtil.createStructType(
          input.getCluster.getTypeFactory,
          childExprs,
          fieldNames,
          SqlValidatorUtil.F_SUGGESTER
        )
        Project.create(
          lateop,
          childExprs,
          rowType,
          lateproj
        )
      case op: TOperator =>
        val rowType = RexUtil.createStructType(
          input.getCluster.getTypeFactory,
          childExprs,
          fieldNames,
          SqlValidatorUtil.F_SUGGESTER
        )
        Project.create(
          op,
          childExprs,
          rowType,
          project
        )
    }
  }

  private[helpers] override val FILTER_FACTORY: RelFactories.FilterFactory =
    (input: RelNode, condition: RexNode, _: util.Set[CorrelationId]) =>
      input match {
        case lateop: TLateOperator =>
          Filter.create(lateop, condition, latefilt)
        case op: TOperator =>
          Filter.create(op, condition, filter)
      }

  private[helpers] override val AGGREGATE_FACTORY
  : RelFactories.AggregateFactory = (
                                      input: RelNode,
                                      _: util.List[RelHint],
                                      groupSet: ImmutableBitSet,
                                      groupSets: ImmutableList[ImmutableBitSet],
                                      aggCalls: util.List[AggregateCall]
                                    ) => {
    assert(
      groupSets == null || (groupSets.size() == 1 && groupSets
        .get(0) == groupSet)
    )
    input match {
      case op: TOperator =>
        Aggregate.create(
          op,
          groupSet,
          aggCalls.asScala.toIndexedSeq.map(a => rex.AggregateCall(a)),
          aggregate
        )
    }
  }

  private[helpers] override val JOIN_FACTORY: RelFactories.JoinFactory = (
                                                                           left: RelNode,
                                                                           right: RelNode,
                                                                           _: util.List[RelHint],
                                                                           condition: RexNode,
                                                                           variablesSet: util.Set[CorrelationId],
                                                                           joinType: JoinRelType,
                                                                           _: Boolean
                                                                         ) => {
    assert(joinType == JoinRelType.INNER)
    assert(variablesSet.size() == 0)
    left match {
      case _: TLateOperator =>
        Join.create(
          left.asInstanceOf[TLateOperator],
          right.asInstanceOf[TLateOperator],
          condition,
          latejoin
        )
      case _: TOperator =>
        Join.create(
          left.asInstanceOf[TOperator],
          right.asInstanceOf[TOperator],
          condition,
          join
        )
    }
  }

  class LateImplementor(storeGenerator: ScannableTable => Store)
    extends Implementor(storeGenerator) {
    def visit(drop: LogicalDrop): RelNode = {
      DROP_FACTORY.createDrop(
        drop.getInput.accept(this)
      )
    }

    def visit(stitch: LogicalStitch): RelNode = {
      STITCH_FACTORY.createStitch(
        stitch.getLeft.accept(this),
        stitch.getRight.accept(this)
      )
    }

    def visit(fetch: LogicalFetch): RelNode = {
      FETCH_FACTORY.createFetch(
        fetch.getInput.accept(this),
        fetch.getFetchType,
        fetch.getColumn,
        fetch.getProjects
      )
    }

    override def visit(other: RelNode): RelNode = {
      other match {
        case drop: LogicalDrop           => visit(drop)
        case stitch: LogicalStitch => visit(stitch)
        case fetch: LogicalFetch => visit(fetch)
        case _                               => super.visit(other)
      }
    }
  }

  override private[helpers] def preprocessPlan(rel: RelNode): RelNode = {
    rel.accept(new RelShuttleImpl() {
      override def visit(table: TableScan): RelNode =
        logical.LogicalDrop.create(
          (0 until table.getRowType.getFieldCount)
            .map(i => {
              ch.epfl.dias.cs422.helpers.store.late.rel.late.volcano.LateColumnScan
                .create(
                  rel.getCluster,
                  table.getTable,
                  tbl => {
                    val store = tbl.getAsLateColumnStore
                    store.getStandaloneColumnAsStore(i).asInstanceOf[Store]
                  },
                  table.getRowType.getFieldList.get(i).getName
                )
                .asInstanceOf[RelNode]
            })
            .reduce((a, b) =>
              logical.LogicalStitch
                .create(
                  a,
                  b,
                  classOf[
                    ch.epfl.dias.cs422.helpers.builder.skeleton.logical.LogicalStitch
                  ]
                )
                .asInstanceOf[RelNode]
            ),
          classOf[
            ch.epfl.dias.cs422.helpers.builder.skeleton.logical.LogicalDrop
          ]
        )

      override def visit(other: RelNode): RelNode = {
        other.copy(
          other.getTraitSet,
          other.getInputs.asScala.map(inp => inp.accept(this)).asJava
        )
      }
    })
  }

  override private[helpers] def implementPlan(
                                               rel: RelNode,
                                               storeType: String
                                             ): RelNode = {
    rel.accept(new LateImplementor(getAsStore(storeType)))
  }

}
