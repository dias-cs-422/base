/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.rex

import ch.epfl.dias.cs422.helpers.rel.RelOperator.{HomogeneousColumn, unwrap}
import org.apache.calcite.DataContext
import org.apache.calcite.adapter.java.JavaTypeFactory
import org.apache.calcite.linq4j.QueryProvider
import org.apache.calcite.schema.SchemaPlus

private[helpers] class TupleContext extends DataContext {
  override def getRootSchema: SchemaPlus =
    throw new RuntimeException("Unreachable code reached")

  override def getTypeFactory: JavaTypeFactory =
    throw new RuntimeException("Unreachable code reached")

  override def getQueryProvider: QueryProvider =
    throw new RuntimeException("Unreachable code reached")

  override def get(name: String): AnyRef =
    throw new RuntimeException("Unreachable code reached")

  def field(index: Int): Any =
    throw new RuntimeException("Unreachable code reached")

  def field2(index: Int): HomogeneousColumn =
    throw new RuntimeException("Unreachable code reached")

  final def field_int(index: Int): Array[Int] =
    unwrap[Int](field2(index))

  final def field_long(index: Int): Array[Long] =
    unwrap[Long](field2(index))

  final def field_boolean(index: Int): Array[Boolean] =
    unwrap[Boolean](field2(index))

  final def field_bigdec(index: Int): Array[java.math.BigDecimal] =
    unwrap[java.math.BigDecimal](field2(index))

  final def field_string(index: Int): Array[java.lang.String] =
    unwrap[java.lang.String](field2(index))

  def size(): Int =
    throw new RuntimeException("Unreachable code reached")
}
