package ch.epfl.dias.cs422.helpers.qo.rules

import ch.epfl.dias.cs422.helpers.builder.skeleton.logical.LogicalDecode
import org.apache.calcite.plan.RelOptRule.{any, operand}
import org.apache.calcite.plan.{RelOptRule, RelOptRuleCall}
import org.apache.calcite.rel.core.Project

import java.util

class ProjectDecodeTransposeRule
    extends RelOptRule(
      // Match Project(logical.LogicalDecode(any...))
      operand(classOf[Project], operand(classOf[LogicalDecode], any)),
      "ProjectDecodeTransposeRule"
    ) {

  override def onMatch(call: RelOptRuleCall): Unit = {
    val proj: Project = call.rel(0)
    val dec: LogicalDecode = call.rel(1)
    call.transformTo(
      LogicalDecode.create(
        proj.copy(
          proj.getTraitSet,
          util.Collections.singletonList(dec.getInput)
        ),
        classOf[LogicalDecode]
      )
    )
  }
}
