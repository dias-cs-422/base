/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers

import ch.epfl.dias.cs422.helpers.SqlPrepare.{cluster, conf, converter}
import ch.epfl.dias.cs422.helpers.builder.Factories
import ch.epfl.dias.cs422.helpers.qo.rules.{JoinPushPredicates, RelDecorrelator}
import ch.epfl.dias.cs422.helpers.rel.RelOperator._
import ch.epfl.dias.cs422.helpers.rel.{RelOperator, RelOperatorUtilLog, early}
import ch.epfl.dias.cs422.helpers.rex.RexExecutor
import ch.epfl.dias.cs422.helpers.store.ScannableTable
import com.google.common.collect.ImmutableList
import org.apache.calcite.jdbc.CalciteSchema
import org.apache.calcite.plan._
import org.apache.calcite.plan.hep.{HepMatchOrder, HepProgramBuilder}
import org.apache.calcite.plan.volcano.VolcanoPlanner
import org.apache.calcite.prepare.CalciteCatalogReader
import org.apache.calcite.rel.`type`.{RelDataType, RelDataTypeSystem}
import org.apache.calcite.rel.metadata.{
  DefaultRelMetadataProvider,
  JaninoRelMetadataProvider,
  RelMetadataQuery,
  RelMetadataQueryBase
}
import org.apache.calcite.rel.rules.CoreRules
import org.apache.calcite.rel.{RelCollations, RelNode}
import org.apache.calcite.sql.`type`.SqlTypeName
import org.apache.calcite.sql.parser.SqlParser
import org.apache.calcite.sql.validate.{SqlValidator, SqlValidatorUtil}
import org.apache.calcite.sql2rel.SqlToRelConverter
import org.apache.calcite.tools.{Frameworks, Programs, RelBuilder}

import java.sql.DriverManager
import java.util

object SqlPrepare {
  private def config: Frameworks.ConfigBuilder = {
    val rootSchema = Frameworks.createRootSchema(true)
    Schema.schema.foreach(e =>
      rootSchema.add(e._1, new ScannableTable(e._2, e._1))
    )
    try {
      val getSchema = Class
        .forName("ch.epfl.dias.cs422.Main")
        .getMethod("getAdditionalSchema")
      getSchema
        .invoke(null)
        .asInstanceOf[Map[String, List[(String, SqlTypeName)]]]
        .foreach(e =>
          if (Schema.schema.contains(e._1)){
            println("Ignoring table (" + e._1 + ") from getAdditionalSchema as it already exists")
          } else {
            rootSchema.add(e._1, new ScannableTable(e._2, e._1))
          }
        )
    } catch {
      case _: Throwable => /* Ignore */
    }
    Frameworks.newConfigBuilder
      .parserConfig(SqlParser.Config.DEFAULT)
      .defaultSchema(rootSchema)
      .programs(
        Programs.heuristicJoinOrder(Programs.RULE_SET, false, 2)
      )
      .typeSystem(RelDataTypeSystem.DEFAULT)
  }

  private val conf =
    SqlPrepare.config.context(Contexts.of(RelBuilder.Config.DEFAULT)).build

  private val builder = {
    if (DriverManager.drivers().findAny().isEmpty) {
      // If calcite driver has been unregistered, re-register
      java.sql.DriverManager.registerDriver(new org.apache.calcite.jdbc.Driver)
    }
    assert(DefaultRelMetadataProvider.INSTANCE != null)
    RelBuilder.create(conf)
  }

  private val catalogReader = new CalciteCatalogReader(
    CalciteSchema.from(conf.getDefaultSchema),
    ImmutableList.of(""),
    builder.getTypeFactory,
    null
  )
  private val validator: SqlValidator = SqlValidatorUtil.newValidator(
    conf.getOperatorTable,
    catalogReader,
    builder.getTypeFactory,
    conf.getSqlValidatorConfig
  )
  val cluster: RelOptCluster = {
    builder.getCluster.setMetadataQuerySupplier(() => {
      if (RelMetadataQueryBase.THREAD_PROVIDERS.get() == null) {
        RelMetadataQueryBase.THREAD_PROVIDERS.set(
          JaninoRelMetadataProvider.of(DefaultRelMetadataProvider.INSTANCE)
        )
      }
      RelMetadataQuery.instance()
    })
    assert(builder.getCluster.getMetadataQuery != null)
    builder.getCluster
  }
  private val converter = new SqlToRelConverter(
    conf.getViewExpander,
    validator,
    catalogReader,
    cluster,
    conf.getConvertletTable,
    conf.getSqlToRelConverterConfig
  )
}

case class SqlPrepare[F <: Factories[_]](
    private val factory: F,
    private val storeType: String,
    private val additionalRules: List[RelOptRule] = List()
) {
  private val planner = {
    val planner = cluster.getPlanner

    planner match {
      case p: VolcanoPlanner => p.setNoneConventionHasInfiniteCost(false)
      case _                 =>
    }

    val rules = planner.getRules
    rules.forEach(x => planner.removeRule(x))

    RelOptUtil.registerAbstractRules(planner)
    planner.addRule(CoreRules.PROJECT_MERGE)
    List(
      //      FilterJoinRule.FILTER_ON_JOIN, // Disable FILTER_ON_JOIN to avoid merging complicated expressions into join
//      CoreRules.FILTER_INTO_JOIN,
      CoreRules.FILTER_REDUCE_EXPRESSIONS,
      CoreRules.JOIN_REDUCE_EXPRESSIONS,
      CoreRules.PROJECT_REDUCE_EXPRESSIONS,
      CoreRules.FILTER_VALUES_MERGE,
      CoreRules.PROJECT_FILTER_VALUES_MERGE,
      CoreRules.PROJECT_VALUES_MERGE,
      CoreRules.AGGREGATE_VALUES
    ).foreach(planner.addRule)

    //    planner.setExecutor(new RexExecutorImpl(Schemas.createDataContext(null, conf.getDefaultSchema)))
    planner.setExecutor(new RexExecutor())

    planner
  }

  private val avgToProjectsProgram = new HepProgramBuilder()
    .addRuleInstance(CoreRules.AGGREGATE_REDUCE_FUNCTIONS)
    .addRuleInstance(CoreRules.PROJECT_MERGE)
    .addRuleInstance(CoreRules.PROJECT_REMOVE)
    .build()
  private val avgToProjects =
    Programs.of(avgToProjectsProgram, false, cluster.getMetadataProvider)

  def prepare(sql: String): RelNode = {
    println("SQL:")
    println(sql)
    // Passes adapted from Proteus

    val parser: SqlParser = SqlParser.create(sql, conf.getParserConfig)
    val sqlQuery = parser.parseQuery

    val log = converter.convertQuery(sqlQuery, true, true).rel

    // Replace LogicalCorrelate nodes with Joins
    val dec = RelDecorrelator.decorrelateQuery(log, SqlPrepare.builder)

    val rexSimplify = Programs.of(
      new HepProgramBuilder()
        .addRuleInstance(CoreRules.FILTER_REDUCE_EXPRESSIONS)
        .addRuleInstance(CoreRules.JOIN_REDUCE_EXPRESSIONS)
        .addRuleInstance(CoreRules.PROJECT_REDUCE_EXPRESSIONS)
        .addRuleInstance(CoreRules.FILTER_VALUES_MERGE)
        .addRuleInstance(CoreRules.PROJECT_FILTER_VALUES_MERGE)
        .addRuleInstance(CoreRules.PROJECT_VALUES_MERGE)
        .addRuleInstance(CoreRules.AGGREGATE_VALUES)
        .build(),
      true,
      cluster.getMetadataProvider
    )

    // Gather together joins as a MultiJoin.
    val hep = new HepProgramBuilder()
      .addRuleInstance(CoreRules.FILTER_INTO_JOIN)
      .addRuleInstance(CoreRules.JOIN_CONDITION_PUSH)
      .addMatchOrder(HepMatchOrder.BOTTOM_UP)
      .addRuleInstance(CoreRules.JOIN_TO_MULTI_JOIN)
      .build()
    val toMultiJoin = Programs.of(hep, true, cluster.getMetadataProvider)

    // Program to expand a MultiJoin into heuristically ordered joins.
    // Do not add JoinCommuteRule and JoinPushThroughJoinRule, as
    // they cause exhaustive search.
    val joinOrdering = Programs.of(
      new HepProgramBuilder()
        .addRuleInstance(CoreRules.MULTI_JOIN_OPTIMIZE)
        .build(),
      true,
      cluster.getMetadataProvider
    )

    // Program to push complicated join conditions, below joins (eg. TPC-H Q19)
    val hepF = Programs.of(
      new HepProgramBuilder()
        .addRuleInstance(JoinPushPredicates.INSTANCE)
        //        .addRuleInstance(CoreRules.JOIN_CONDITION_PUSH)
        //        .addRuleInstance(CoreRules.JOIN_EXTRACT_FILTER)
        .build(),
      true,
      cluster.getMetadataProvider
    )

    val rj = Programs
      .sequence(
        // Simplify rex'es
        rexSimplify,
        // Join reordering
        toMultiJoin,
        //      print,
        joinOrdering,
        // Move filters below joins
        hepF
      )
      .run(
        planner,
        dec,
        cluster.traitSet(),
        util.List.of[RelOptMaterialization](),
        util.List.of[RelOptLattice]()
      )
    //    PrintUtil.printTree(rj)

    // Optimize using a Volcano Optimizer
    planner.setRoot(rj)
    val r = planner.findBestExp()
    //    PrintUtil.printTree(r)

    // Convert Aggregates with AVG, to Project(Aggregate(Sum, Cnt))
    var rel = avgToProjects.run(
      planner,
      r,
      cluster.traitSet(),
      util.List.of[RelOptMaterialization](),
      util.List.of[RelOptLattice]()
    )

    rel = factory.preprocessPlan(rel)

    PrintUtil.printTree(rel)

    if (additionalRules.nonEmpty) {
      val hepBuilder = new HepProgramBuilder().addGroupBegin()
//      hepBuilder.addMatchOrder(HepMatchOrder.TOP_DOWN)
      additionalRules.foreach(rule => hepBuilder.addRuleInstance(rule))

      rel = Programs
        .of(hepBuilder.addGroupEnd().build(), true, cluster.getMetadataProvider)
        .run(
          planner,
          rel,
          cluster.traitSet(),
          util.List.of[RelOptMaterialization](),
          util.List.of[RelOptLattice]()
        )

      PrintUtil.printTree(rel)
    }

    // Force changing Logical operators to our operators
    val out = factory.implementPlan(rel, storeType)
    PrintUtil.printTree(out)
    out
  }

  def runQuery(sql: String): (List[IndexedSeq[Any]], RelDataType, Boolean) = {
    RelOperatorUtilLog.resetFieldAccessCount()
    try {
      val operator =
        PrintUtil.time(
          () => {
            prepare(sql)
          },
          "Prepare time"
        )
      PrintUtil.printTree(operator)
      val tmp = PrintUtil.time(
        () => {
          (
            operator match {
              case op: ch.epfl.dias.cs422.helpers.rel.late.operatoratatime.Operator =>
                val evals = op.evaluators()
                val x = op.execute().map(n => evals(n)).toList
                x
              case op: ch.epfl.dias.cs422.helpers.rel.early.blockatatime.Operator =>
                val res = op.toList
                res.foreach(b =>
                  if (b.size > RelOperator.blockSize)
                    throw new AssertionError("Block size exceeded")
                )
                res.flatten
              case op: ch.epfl.dias.cs422.helpers.rel.early.operatoratatime.Operator =>
                op.execute()
                  .transpose
                  .filter(t => t.last.asInstanceOf[Boolean])
                  .map(t => t.take(t.size - 1))
                  .toList
              case op: ch.epfl.dias.cs422.helpers.rel.early.columnatatime.Operator =>
                op.execute()
                  .transpose
                  .filter(t => t.last.asInstanceOf[Boolean])
                  .map(t => t.take(t.size - 1))
                  .toList
              case op: early.volcano.Operator => op.toList
            },
            operator.getRowType,
            operator.getTraitSet.containsIfApplicable(RelCollations.EMPTY)
          )
        },
        "Execution time"
      )
      println("Total accesses: " + RelOperatorUtilLog.getFieldAccessCount)
      tmp
    } catch {
      case e: Throwable =>
        println("Total accesses: -1")
        throw e
    }
  }
}
