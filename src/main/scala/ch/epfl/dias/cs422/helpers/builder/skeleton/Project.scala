/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.builder.skeleton

import java.util

import ch.epfl.dias.cs422.helpers.Construct
import org.apache.calcite.plan.RelTraitSet
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rel.{RelNode, core}
import org.apache.calcite.rex.RexNode

/** Transforms the input tuples for example from Tuple(a, b) to Tuple(a + 4, b/3, "hello", if (a < 3) b * 2 else 0)
  *
  * @param projects describes each output attribute.
  * @group helpers
  */
abstract class Project[TOperator <: RelNode] protected (
    input: TOperator,
    projects: util.List[_ <: RexNode],
    rowType: RelDataType
) extends core.Project(
      input.getCluster,
      input.getTraitSet,
      input,
      projects,
      rowType
    ) {
  self: TOperator =>
  final override def copy(
      traitSet: RelTraitSet,
      input: RelNode,
      projects: util.List[RexNode],
      rowType: RelDataType
  ): Project[TOperator] = {
    Project.create(
      input.asInstanceOf[TOperator],
      projects,
      rowType,
      this.getClass
    )
  }
}

private[helpers] object Project {
  def create[TOperator <: RelNode](
      input: TOperator,
      projects: java.util.List[_ <: RexNode],
      rowType: RelDataType,
      c: Class[_ <: Project[TOperator]]
  ): Project[TOperator] = {
    Construct.create(c, input, projects, rowType)
  }
}
