/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.qo.rules

import ch.epfl.dias.cs422.helpers.builder.skeleton.logical.{
  LogicalDecode,
  LogicalReconstruct
}
import org.apache.calcite.plan.RelOptRule.{any, operand}
import org.apache.calcite.plan.{RelOptRule, RelOptRuleCall}
import org.apache.calcite.rel.RelNode
import org.apache.calcite.rel.core.Project
import org.apache.calcite.rex.RexNode

class ProjectReconstructTransposeRule
    extends RelOptRule(
      // Match Project(logical.Decode(any...))
      operand(classOf[Project], operand(classOf[LogicalReconstruct], any)),
      "ProjectReconstructTransposeRule"
    ) {

  override def onMatch(call: RelOptRuleCall): Unit = {
    val proj: Project = call.rel(0)
    val recons: LogicalReconstruct = call.rel(1)
    val pushProject = new PushProjector(
      proj,
      null,
      recons,
      (expr: RexNode) => true,
      call.builder
    )

    if (pushProject.locateAllRefs) {
      return
    }

    val leftProjRel = pushProject
      .createProjectRefsAndExprs(recons.getLeft, true, false)
    val rightProjRel = pushProject
      .createProjectRefsAndExprs(recons.getRight, true, true)

    val topProject: RelNode = pushProject.createNewProject(
      LogicalReconstruct.create(
        leftProjRel,
        rightProjRel
      ),
      pushProject.getAdjustments
    )

    call.transformTo(topProject)
  }
}
