package ch.epfl.dias.cs422.helpers.store.rle.rel.early.volcano

import ch.epfl.dias.cs422.helpers.Construct
import ch.epfl.dias.cs422.helpers.builder.skeleton
import ch.epfl.dias.cs422.helpers.rel.RelOperator.{NilRLEentry, RLEentry}
import ch.epfl.dias.cs422.helpers.store.rle.RLEStandaloneColumnStore
import ch.epfl.dias.cs422.helpers.store.{ScannableTable, Store}
import org.apache.calcite.plan.{RelOptCluster, RelOptTable, RelTraitSet}
import org.apache.calcite.rel.RelWriter
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rel.metadata.RelMetadataQuery

import java.util.Collections

/**
  * @inheritdoc
  * @see [[ch.epfl.dias.cs422.helpers.builder.skeleton.Scan]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.early.volcano.Operator]]
  */
class RLEColumnScan protected (
    cluster: RelOptCluster,
    traitSet: RelTraitSet,
    table: RelOptTable,
    val tableToStore: ScannableTable => Store,
    val colName: String
) extends skeleton.Scan[
      ch.epfl.dias.cs422.helpers.rel.early.volcano.rle.Operator
    ](cluster, traitSet, table)
    with ch.epfl.dias.cs422.helpers.rel.early.volcano.rle.Operator {
  override def toString = "RLEColumnScan"

  protected val scannable: RLEStandaloneColumnStore = tableToStore(
    table.unwrap(classOf[ScannableTable])
  ).asInstanceOf[RLEStandaloneColumnStore]

  final override def estimateRowCount(mq: RelMetadataQuery): Double = {
    scannable.getRowCount / 5
  }

  private var current = 0;
  private var consumed = 0;
  private lazy val col = scannable.getRLEColumn

  override def deriveRowType: RelDataType = {
    val v = table.getRowType.getFieldList.get(scannable.getColumnIndex)
    getCluster.getTypeFactory.createStructType(
      Collections.singletonList(v.getType),
      Collections.singletonList(v.getName)
    )
  }

  override def explainTerms(pw: RelWriter): RelWriter = {
    pw.item("column", colName)
    super.explainTerms(pw)
  }

  /**
    * @inheritdoc
    */
  override def open(): Unit = {
    current = 0
    consumed = 0
  }

  /**
    * @inheritdoc
    */
  override def next(): Option[RLEentry] = {
    if (consumed < scannable.getRowCount) {
      val v = col(current)
      current += 1
      consumed += v.length.asInstanceOf[Int]
      Option(v)
    } else {
      NilRLEentry
    }
  }

  /**
    * @inheritdoc
    */
  override def close(): Unit = {}
}

private[helpers] object RLEColumnScan {
  def create(
      cluster: RelOptCluster,
      table: RelOptTable,
      tableToStore: ScannableTable => Store,
      colName: String
  ): RLEColumnScan =
    Construct.create(
      classOf[RLEColumnScan],
      cluster,
      cluster.traitSet(),
      table,
      tableToStore,
      colName
    )
}
