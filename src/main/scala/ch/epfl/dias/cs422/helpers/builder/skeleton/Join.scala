/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.builder.skeleton

import java.util
import ch.epfl.dias.cs422.helpers.Construct
import org.apache.calcite.plan.{RelOptCost, RelOptPlanner, RelTraitSet}
import org.apache.calcite.rel.core.{CorrelationId, JoinRelType}
import org.apache.calcite.rel.metadata.RelMetadataQuery
import org.apache.calcite.rel.{RelNode, core}
import org.apache.calcite.rex.RexNode

import scala.jdk.CollectionConverters._

/**
  * The output of the Join operator should be the fields of the left child followed by the fields of the right child
  * The join is always an ''equijoin'' with the condition comparing the fields of the left child at indices [[getLeftKeys]]
  * with the fields of the right child at indices [[getRightKeys]].
  *
  * @param left      Left input of the join
  * @param right     Right input of the join
  * @param condition represents the equality condition for the join. Do not use it directly, but go through
  *                  the [[getLeftKeys]], [[getRightKeys]] functions.
  *
  * @see [[ch.epfl.dias.cs422.helpers.rel.RelOperator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.late.LazyOperator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.early.blockatatime.Operator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.early.operatoratatime.Operator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.early.volcano.Operator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.late.operatoratatime.Operator]]
  *
  * @group helpers
  */
abstract class Join[TOperator <: RelNode] protected (
    left: TOperator,
    right: TOperator,
    condition: RexNode
) extends core.Join(
      left.getCluster,
      left.getTraitSet,
      util.List.of(),
      left,
      right,
      condition,
      util.Set.of[CorrelationId](),
      JoinRelType.INNER
    ) {
  self: TOperator =>

  private lazy val analyzeCond = analyzeCondition()

  /**
    * Returns the indices of the fields of the left input's tuples that participate in the key.
    * The return order matches the order of [[getRightKeys]]
    *
    * @return Returns the indices of the fields of the left input's tuples that participate in the key.
    *
    * @group helpers
    */
  protected final lazy val getLeftKeys: IndexedSeq[Int] = {
    analyzeCond.leftKeys.asScala.toIndexedSeq.map(i => i.toInt)
  }

  /** Computes the cost of the current operator.
    *
    * Used by the metadata provides for estimating the cost of this operator.
    * Metadata providers may overwrite it.
    */
  override def computeSelfCost(
      planner: RelOptPlanner,
      mq: RelMetadataQuery
  ): RelOptCost = {
    if (!analyzeCond.isEqui) {
      getCluster.getPlanner.getCostFactory.makeHugeCost()
    } else {
      super.computeSelfCost(planner, mq)
    }
  }

  /**
    * Returns the indices of the fields of the right input's tuples that participate in the key.
    * The return order matches the order of [[getLeftKeys]]
    *
    * @return Returns the indices of the fields of the right input's tuples that participate in the key.
    *
    * @group helpers
    */
  protected final lazy val getRightKeys = {
    analyzeCond.rightKeys.asScala.toIndexedSeq.map(i => i.toInt)
  }

  final override def copy(
      traitSet: RelTraitSet,
      condition: RexNode,
      left: RelNode,
      right: RelNode,
      joinType: JoinRelType,
      semiJoinDone: Boolean
  ): Join[TOperator] = {
    Join.create(
      left.asInstanceOf[TOperator],
      right.asInstanceOf[TOperator],
      condition,
      this.getClass
    )
  }
}

private[helpers] object Join {
  def create[TOperator <: RelNode](
      left: TOperator,
      right: TOperator,
      condition: RexNode,
      c: Class[_ <: Join[TOperator]]
  ): Join[TOperator] = {
    Construct.create(c, left, right, condition)
  }
}
