package ch.epfl.dias.cs422.helpers.builder.skeleton.logical

import ch.epfl.dias.cs422.helpers.Construct
import org.apache.calcite.plan.RelTraitSet
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rel.metadata.RelMetadataQuery
import org.apache.calcite.rel.{BiRel, RelNode}
import org.apache.calcite.sql.validate.SqlValidatorUtil

import java.util
import java.util.Collections

/** Reconstructs [[ch.epfl.dias.cs422.helpers.rel.RelOperator.Tuple]]s from [[ch.epfl.dias.cs422.helpers.rel.RelOperator.LateTuple]]s
  *
  * For example, if left produces:
  *
  * {{{ LateTuple(1, Tuples("a", 5)), LateTuple(10, Tuples("b", 6)), LateTuple(20, Tuples("c", 7)) }}}
  *
  * and right produces:
  *
  * {{{ LateTuple(0, Tuples(3.0)), LateTuple(20, Tuples(4.0)) }}}
  *
  * Stitch should produce:
  *
  * {{{ LateTuple(20, Tuples("c", 7, 4.0)) }}}
  *
  * You may assume that each of [[left]] and [[right]] produces [[ch.epfl.dias.cs422.helpers.rel.RelOperator.LateTuple]]s sorted
  * by vid and non-overlapping, but they may "miss" tuples due to filtering or other operations.
  *
  * Output Tuples contained in the LateTuple start with the attributes of [[left]] followed by the attributes of [[right]]
  *
  * @param left  Left input
  * @param right Right input
  */
class LogicalStitch protected (
                                     left: RelNode,
                                     right: RelNode
                                   ) extends BiRel(left.getCluster, left.getTraitSet, left, right) {

  final override def estimateRowCount(mq: RelMetadataQuery): Double = {
    Math.min(mq.getRowCount(left), mq.getRowCount(right))
  }

  final override def deriveRowType: RelDataType = {
    SqlValidatorUtil.createJoinType(
      getCluster.getTypeFactory,
      left.getRowType,
      right.getRowType,
      null,
      Collections.emptyList
    )
  }

  override def copy(
                     traitSet: RelTraitSet,
                     inputs: util.List[RelNode]
                   ): LogicalStitch = {
    assert(inputs.size() == 2)
    copy(
      inputs.get(0),
      inputs.get(1)
    )
  }

  def copy(
            left: RelNode,
            right: RelNode
          ): LogicalStitch = {
    LogicalStitch.create(
      left,
      right,
      this.getClass
    )
  }
}

object LogicalStitch {
  def create(left: RelNode, right: RelNode): LogicalStitch = {
    create(left, right, classOf[LogicalStitch])
  }

  private[helpers] def create(
                               left: RelNode,
                               right: RelNode,
                               c: Class[_ <: LogicalStitch]
                             ): LogicalStitch = {
    Construct.create(c, left, right)
  }
}
