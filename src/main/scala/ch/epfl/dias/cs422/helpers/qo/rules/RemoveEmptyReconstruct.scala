package ch.epfl.dias.cs422.helpers.qo.rules

import ch.epfl.dias.cs422.helpers.builder.skeleton.logical.LogicalReconstruct
import ch.epfl.dias.cs422.helpers.store.rle.rel.early.volcano.RLEColumnScan
import org.apache.calcite.plan.{RelOptRuleCall, RelRule}
import org.apache.calcite.rel.RelNode
import org.apache.calcite.rel.logical.LogicalProject

/**
  * Removes a child of a Reconstruct if it's not used.
  * That is, if the input is not filtering the tuples
  * and only scans a column that does not participate
  * in any projections.
  *
  * @param config configuration parameters
  */
class RemoveEmptyReconstructLeft protected (
    config: RelRule.Config
) extends RelRule[RelRule.Config](config) {

  override def matches(call: RelOptRuleCall): Boolean = {
    if (!call.rel(1).asInstanceOf[LogicalProject].getProjects.isEmpty) false
    else super.matches(call)
  }

  override def onMatch(call: RelOptRuleCall): Unit = {
    val reco: LogicalReconstruct = call.rel(0)
    call.transformTo(reco.getRight)
  }
}

/**
  * Removes a child of a Reconstruct if it's not used.
  * That is, if the input is not filtering the tuples
  * and only scans a column that does not participate
  * in any projections.
  *
  * @param config configuration parameters
  */
class RemoveEmptyReconstructRight protected (
    config: RelRule.Config
) extends RelRule[RelRule.Config](config) {

  override def matches(call: RelOptRuleCall): Boolean = {
    if (!call.rel(2).asInstanceOf[LogicalProject].getProjects.isEmpty) false
    else super.matches(call)
  }

  override def onMatch(call: RelOptRuleCall): Unit = {
    val reco: LogicalReconstruct = call.rel(0)
    call.transformTo(reco.getLeft)
  }
}

object RemoveEmptyReconstructLeft {

  /**
    * Configuration for a [[RemoveEmptyReconstructLeft]]
    */
  val INSTANCE = new RemoveEmptyReconstructLeft(
    // By default, get an empty configuration
    RelRule.Config.EMPTY
    // and match:
      .withOperandSupplier((b: RelRule.OperandBuilder) =>
        // A node of class classOf[LogicalFilter]
        b.operand(classOf[LogicalReconstruct])
          // that has inputs:
          .oneInput(b1 =>
            // A node that is a LogicalDecode
            b1.operand(classOf[LogicalProject])
              // of one input
              .oneInput(b2 =>
                // A node that is a LogicalDecode
                b2.operand(classOf[RLEColumnScan])
                  // of no input
                  .noInputs()
              )
          )
      )
  )
}

object RemoveEmptyReconstructRight {

  /**
    * Configuration for a [[RemoveEmptyReconstructRight]]
    */
  val INSTANCE = new RemoveEmptyReconstructRight(
    // By default, get an empty configuration
    RelRule.Config.EMPTY
    // and match:
      .withOperandSupplier((b: RelRule.OperandBuilder) =>
        // A node of class classOf[LogicalFilter]
        b.operand(classOf[LogicalReconstruct])
          // that has inputs:
          .inputs(
            bany =>
              // Any node
              bany
                .operand(classOf[RelNode])
                // with any inputs
                .anyInputs(),
            b1 =>
              // A node that is a LogicalDecode
              b1.operand(classOf[LogicalProject])
                // of one input
                .oneInput(b2 =>
                  // A node that is a LogicalDecode
                  b2.operand(classOf[RLEColumnScan])
                    // of no input
                    .noInputs()
                )
          )
      )
  )

}
