/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers

import java.io.InputStream
import java.nio.file._
import java.util.Collections

object PathUtils {
  private def prefixPath(path: String) = {
    (if (path.charAt(0) == '/') "" else "/") + path
  }

  private def jarPath(path: String) = {
    prefixPath(path.replace('\\', '/'))
  }

  def getResourceAsStream(path: String, c: Class[_] = getClass): InputStream = {
    var r =
      c.getResourceAsStream(prefixPath(path))
    if (r == null) {
      r = c.getResourceAsStream(jarPath(path))
    }
    if (r == null) {
      Files.newInputStream(getResourceAsPath(path, c))
    } else {
      r
    }
  }

  def getResourceAsPath(
      path: String,
      c: Class[_] = getClass
  ): Path = {
    var p = c.getResource(prefixPath(path))
    if (p == null) {
      p = c.getResource(jarPath(path))
    }
    if (p == null) {
      // If no such file, fall back to the a normal, relative path, outside of resources
      Paths.get(path)
    } else {
      if (p.toURI.getScheme == "jar") {
        {
          try {
            FileSystems
              .newFileSystem(p.toURI, Collections.emptyMap[String, AnyRef]())
          } catch {
            case _: FileSystemAlreadyExistsException =>
              FileSystems.getFileSystem(p.toURI)
          }
        }.getPath(path)
      } else {
        Paths.get(p.getPath)
      }
    }
  }
}
