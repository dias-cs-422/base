package ch.epfl.dias.cs422.helpers.rel.late.volcano.naive

import ch.epfl.dias.cs422.helpers.rel.RelOperator
import ch.epfl.dias.cs422.helpers.rel.RelOperator.LateTuple

trait Operator extends RelOperator with Iterable[LateTuple] {
  def open(): Unit

  def next(): Option[LateTuple]

  /** Allows the Operator to clean up its resources.
    *
    * Every call to [[open]] should be matched to a call to [[close]] and vice versa.
    * Furthermore, after a call to [[close]], the Operator will only accept calls to [[open]].
    * Calling [[next]] after [[close]], without re-[[open]]ing the operator,
    * has undefined behavior.
    *
    * @group helpers
    */
  def close(): Unit

  /** Returns an iterator to iterate over the Late materialized tuples produced by the operator.
    *
    * The iterator takes care of opening and closing the operator.
    *
    * @return Iterator over the tuples produced by the current operator.
    *
    * @group helpers
    */
  final override def iterator: Iterator[LateTuple] =
    new Iterator[LateTuple] with AutoCloseable {
      private val op =
        Operator.this.copy(getTraitSet, getInputs).asInstanceOf[Operator]
      op.open()

      var n: Option[Option[LateTuple]] = Option.empty

      def prepareNext(): Unit = {
        if (n.nonEmpty) return
        n = Option(op.next())
      }

      override def hasNext: Boolean = {
        prepareNext()
        n.get.nonEmpty
      }

      override def next(): LateTuple = {
        prepareNext()
        val ret = n.get
        assert(ret.nonEmpty)
        n = Option.empty
        ret.get
      }

      override def close(): Unit = {
        op.close()
      }
    }
}
