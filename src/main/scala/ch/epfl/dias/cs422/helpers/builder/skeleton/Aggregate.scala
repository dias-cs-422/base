/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.builder.skeleton

import java.util

import ch.epfl.dias.cs422.helpers.Construct
import ch.epfl.dias.cs422.helpers.rex.AggregateCall
import org.apache.calcite.plan.RelTraitSet
import org.apache.calcite.rel.{RelNode, core}
import org.apache.calcite.util.ImmutableBitSet

import scala.jdk.CollectionConverters._

/**
  * The output of the aggregates contains the grouping keys as the first fields followed by the aggregates.
  * The grouping keys are at the indices pointed by groupSet.
  *
  *
  * A possible pseudo-implementation of the Aggregate would be the following:
  *
  *  1. If input is empty and the group by clause is empty:
  *     i. return aggEmptyValue for each aggregate.
  *  1. Otherwise:
  *     i. Group based on the key produced by the indices in groupSet
  *     i. For each group and each aggregate call "agg":
  *        a. find the input of the aggregate by calling [[AggregateCall.getArgument agg.getArgument]].
  *        a. reduce the values in the group by picking pairs and applying the [[AggregateCall.reduce agg.reduce]] function.
  *        a. Conclude a group as soon as you have a single value.
  *
  *
  * @example
  * {{{
  * SELECT COUNT(*), SUM(x), MIN(y) FROM A ;
  *
  * Key: groupSet = [] (empty) (what I referenced as reduction)
  *
  * AggregateCalls: {COUNT, getArgument=1, reduce=_+_, aggEmptyValue=0},  {SUM, getArgument="get x", reduce=_+_, aggEmptyValue=null}, {MIN, getArgument="get y", reduce=min, aggEmptyValue=null}
  *
  * Possible outputs:
  * case1: (0, NULL, NULL)
  * case2: (5, 1241, -40293)
  * }}}
  * @example
  * {{{
  * SELECT z, w, COUNT(*), SUM(x), MIN(y) FROM A GROUP BY z, w ;
  *
  * Key: groupSet = [Index of z, Index of w]
  *
  * Aggregate Calls: {COUNT, getArgument=1, reduce=_+_, aggEmptyValue=0},  {SUM, getArgument="get x", reduce=_+_, aggEmptyValue=null}, {MIN, getArgument="get y", reduce=min, aggEmptyValue=null}
  *
  * Possible outputs:
  *
  * case1: [(5, 7, 1, 15, 20), (3, 7, 6, 1039123, 0)]
  * case2: []
  * }}}
  * @param aggrGroupSet Contains the indices of the fields of each tuple that will participate in the key (group by clause) of the aggregation.
  *                 Thus, a groupSet with set bits at 0, 3, 7 means that from a tuple (A0, A1, A2, A3, A4, A5, A6, A7) your grouping key is (A0, A3, A7).
  *                 This holds for all the input tuples.
  * @param aggrCalls A list of aggregation functions (like COUNT(*), COUNT(x), MIN(y), SUM(z) etc.)
  * @see [[ch.epfl.dias.cs422.helpers.rel.RelOperator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.late.LazyOperator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.early.blockatatime.Operator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.early.operatoratatime.Operator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.early.volcano.Operator]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.late.operatoratatime.Operator]]
  * @group helpers
  */
abstract class Aggregate[TOperator <: RelNode] protected (
    input: TOperator,
    groupSet: ImmutableBitSet,
    aggCalls: IndexedSeq[AggregateCall]
) extends core.Aggregate(
      input.getCluster,
      input.getTraitSet,
      input,
      groupSet,
      List(groupSet).asJava,
      aggCalls.map(Aggregate.unwarp).asJava
    ) {
  self: TOperator =>

  final override def copy(
      traitSet: RelTraitSet,
      input: RelNode,
      groupSet: ImmutableBitSet,
      groupSets: util.List[ImmutableBitSet],
      aggCalls: util.List[core.AggregateCall]
  ): Aggregate[TOperator] = {
    Aggregate.create(
      input.asInstanceOf[TOperator],
      groupSet,
      aggCalls.asScala.map(a => AggregateCall(a)).toIndexedSeq,
      this.getClass
    )
  }
}

private[helpers] object Aggregate {
  def create[TOperator <: RelNode](
      input: TOperator,
      groupSet: ImmutableBitSet,
      aggCalls: IndexedSeq[AggregateCall],
      c: Class[_ <: Aggregate[TOperator]]
  ): Aggregate[TOperator] = {
    Construct.create(c, input.asInstanceOf[TOperator], groupSet, aggCalls)
  }

  private def unwarp(aggregateCall: AggregateCall): core.AggregateCall = {
    aggregateCall.agg
  }
}
