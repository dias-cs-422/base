/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.builder.skeleton

import ch.epfl.dias.cs422.helpers.Construct
import org.apache.calcite.plan.RelTraitSet
import org.apache.calcite.rel.{core, _}
import org.apache.calcite.rex.RexNode

/**
 * Filters the input and returns the tuples for whom `condition` evaluates to true.
 *
 * @param input     Input of Filter
 * @param condition Filtering condition. The evaluated conditions is guaranteed to be a [[scala.Boolean]]
 *
 * @see [[ch.epfl.dias.cs422.helpers.rel.RelOperator]]
 * @see [[ch.epfl.dias.cs422.helpers.rel.late.LazyOperator]]
 * @see [[ch.epfl.dias.cs422.helpers.rel.early.blockatatime.Operator]]
 * @see [[ch.epfl.dias.cs422.helpers.rel.early.operatoratatime.Operator]]
 * @see [[ch.epfl.dias.cs422.helpers.rel.early.volcano.Operator]]
 * @see [[ch.epfl.dias.cs422.helpers.rel.late.operatoratatime.Operator]]
 *
 * @group helpers
 */
abstract class Filter[TOperator <: RelNode] protected (input: TOperator, condition: RexNode) extends core.Filter(input.getCluster, input.getTraitSet, input, condition) {
  self: TOperator =>
  final override def copy(traitSet: RelTraitSet, input: RelNode, condition: RexNode): Filter[TOperator] = Filter.create(input.asInstanceOf[TOperator], condition, this.getClass)
}

private[helpers] object Filter{
  def create[TOperator <: RelNode](input: TOperator, condition: RexNode, c: Class[_ <: Filter[TOperator]]): Filter[TOperator] = {
    Construct.create(c, input, condition)
  }
}