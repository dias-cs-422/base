package ch.epfl.dias.cs422.helpers.store.late

import ch.epfl.dias.cs422.helpers.rel.RelOperator._
import ch.epfl.dias.cs422.helpers.rel.RelOperatorUtilLog
import ch.epfl.dias.cs422.helpers.store.Store

class LateColumnStore private[store] (
                                   private val data: IndexedSeq[Column],
                                   private val rowCount: Long
                                 ) extends Store {
  private def getData(i: Int): Column = {
    if (rowCount == 0) IndexedSeq()
    else data(i)
  }

  def getColumn(i: Int): Column = {
    val tmp = getData(i)
    RelOperatorUtilLog.accesses = RelOperatorUtilLog.accesses + tmp.size
    tmp
  }

  override def getRowCount: Long =
    rowCount // otherwise we can't retrieve it for 0-column tables

  private[helpers] def getStandaloneColumnAsStore(i: Int): LateStandaloneColumnStore = {
    new LateStandaloneColumnStore(getData(i), getRowCount, i)
  }
}

