package ch.epfl.dias.cs422.helpers.builder.skeleton.logical

import ch.epfl.dias.cs422.helpers.Construct
import org.apache.calcite.plan.RelTraitSet
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rel.metadata.RelMetadataQuery
import org.apache.calcite.rel.{BiRel, RelNode}
import org.apache.calcite.sql.validate.SqlValidatorUtil

import java.util
import java.util.Collections

/** Reconstructs [[ch.epfl.dias.cs422.helpers.rel.RelOperator.Tuple]]s from [[ch.epfl.dias.cs422.helpers.rel.RelOperator.RLEentry]]s
  *
  * For example, if left produces:
  *
  * {{{ RLEentry(1, 2, Tuples("a", 5)), RLEentry(10, 3, Tuples("b", 6)), RLEentry(20, 1, Tuples("c", 7)) }}}
  *
  * and right produces:
  *
  * {{{ RLEentry(0, 11, Tuples(3.0)), RLEentry(20, 1000, Tuples(4.0)) }}}
  *
  * Reconstruct should produce:
  *
  * {{{ RLEentry(1, 2, Tuples("a", 5, 3.0)), RLEentry(10, 1, Tuples("b", 6, 3.0)), RLEentry(20, 1, Tuples("c", 7, 4.0)) }}}
  *
  * You may assume that each of [[left]] and [[right]] produces [[ch.epfl.dias.cs422.helpers.rel.RelOperator]]s sorted
  * by startVID and non-overlapping, but they may "miss" tuples due to filtering or other operations.
  *
  * Output Tuples contained in the RLEentry start with the attributes of [[left]] followed by the attributes of [[right]]
  *
  * @param left  Left input
  * @param right Right input
  */
class LogicalReconstruct protected (
    left: RelNode,
    right: RelNode
) extends BiRel(left.getCluster, left.getTraitSet, left, right) {

  final override def estimateRowCount(mq: RelMetadataQuery): Double = {
    Math.min(mq.getRowCount(left), mq.getRowCount(right))
  }

  final override def deriveRowType: RelDataType = {
    SqlValidatorUtil.createJoinType(
      getCluster.getTypeFactory,
      left.getRowType,
      right.getRowType,
      null,
      Collections.emptyList
    )
  }

  override def copy(
      traitSet: RelTraitSet,
      inputs: util.List[RelNode]
  ): LogicalReconstruct = {
    assert(inputs.size() == 2)
    copy(
      inputs.get(0),
      inputs.get(1)
    )
  }

  def copy(
      left: RelNode,
      right: RelNode
  ): LogicalReconstruct = {
    LogicalReconstruct.create(
      left,
      right,
      this.getClass
    )
  }
}

object LogicalReconstruct {
  def create(left: RelNode, right: RelNode): LogicalReconstruct = {
    create(left, right, classOf[LogicalReconstruct])
  }

  private[helpers] def create(
      left: RelNode,
      right: RelNode,
      c: Class[_ <: LogicalReconstruct]
  ): LogicalReconstruct = {
    Construct.create(c, left, right)
  }
}
