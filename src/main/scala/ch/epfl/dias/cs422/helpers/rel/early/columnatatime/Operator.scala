/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers.rel.early.columnatatime

import ch.epfl.dias.cs422.helpers.rel.RelOperator
import ch.epfl.dias.cs422.helpers.rel.RelOperator.HomogeneousColumn

trait Operator extends RelOperator {
  /**
   * Calculates the output of evaluating the operator, in a columnar format.
   * The last [[HomogeneousColumn]] should be a selection vector, that is, a
   * [[HomogeneousColumn]] of [[Boolean]] being ''true'' for the tuples that
   * are active and ''false'' otherwise.
   *
   * @return Return the result of evaluating the operator
   *
   * @group helpers
   */
  def execute(): IndexedSeq[HomogeneousColumn]
}
