package ch.epfl.dias.cs422.helpers.builder.skeleton.logical

import ch.epfl.dias.cs422.helpers.Construct
import org.apache.calcite.plan.RelTraitSet
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rel.metadata.RelMetadataQuery
import org.apache.calcite.rel.{RelNode, SingleRel}

import java.util

/** Decodes RLE tuples to normal tuples.
  *
  * For example, if the input operator produces: RLEentry(4, 2, Tuple("a", 5)), RLEentry(8, 3, Tuple("b", 6)), then
  * the Decode operator should produce: Tuple("a", 5), Tuple("a", 5), Tuple("b", 6), Tuple("b", 6), Tuple("b", 6)
  *
  * @param input Operator that produces RLE entries
  */
class LogicalDecode protected(
    input: RelNode
) extends SingleRel(input.getCluster, input.getTraitSet, input) {

  final override def deriveRowType: RelDataType = {
    input.getRowType
  }

  final override def estimateRowCount(mq: RelMetadataQuery): Double = {
    mq.getRowCount(input) * 5
  }

  override def copy(
      traitSet: RelTraitSet,
      inputs: util.List[RelNode]
  ): LogicalDecode = {
    assert(inputs.size == 1)
    copy(
      inputs.get(0)
    )
  }

  def copy(
      input: RelNode
  ): LogicalDecode = {
    LogicalDecode.create(
      input,
      this.getClass
    )
  }
}

private[helpers] object LogicalDecode {
  def create(
      input: RelNode,
      c: Class[_ <: LogicalDecode]
  ): LogicalDecode = {
    Construct.create(c, input)
  }
}
