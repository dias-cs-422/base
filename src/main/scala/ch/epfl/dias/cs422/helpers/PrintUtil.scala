/*
    Utilities for CS422 project on query execution and optimization

                            Copyright (c) 2020
        Data Intensive Applications and Systems Laboratory (DIAS)
                École Polytechnique Fédérale de Lausanne

                            All Rights Reserved.

    Permission to use, copy, modify and distribute this software and
    its documentation is hereby granted, provided that both the
    copyright notice and this permission notice appear in all copies of
    the software, derivative works or modified versions, and any
    portions thereof, and that both notices appear in supporting
    documentation.

    This code is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS
    DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER
    RESULTING FROM THE USE OF THIS SOFTWARE.
 */

package ch.epfl.dias.cs422.helpers

import org.apache.calcite.plan.RelOptUtil
import org.apache.calcite.rel.RelNode
import org.apache.calcite.sql.SqlExplainLevel

private[helpers] object PrintUtil {
  def printTree(rel: RelNode): Unit = {
    print(RelOptUtil.toString(rel, SqlExplainLevel.ALL_ATTRIBUTES))
  }

  def time[T](f: () => T, message: String = null): T = {
    val startTimeMillis = System.currentTimeMillis()
    val ret = f()
    val endTimeMillis = System.currentTimeMillis()
    println(
      (if (message != null) message + ": "
       else "") + (endTimeMillis - startTimeMillis) + "ms"
    )
    ret
  }
}
