package ch.epfl.dias.cs422.helpers.store.late.rel.late.volcano

import ch.epfl.dias.cs422.helpers.Construct
import ch.epfl.dias.cs422.helpers.builder.skeleton
import ch.epfl.dias.cs422.helpers.rel.RelOperator.{LateTuple, NilLateTuple}
import ch.epfl.dias.cs422.helpers.store.late.LateStandaloneColumnStore
import ch.epfl.dias.cs422.helpers.store.{ScannableTable, Store}
import org.apache.calcite.plan.{RelOptCluster, RelOptTable, RelTraitSet}
import org.apache.calcite.rel.RelWriter
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rel.metadata.RelMetadataQuery

import java.util.Collections

/**
  * @inheritdoc
  * @see [[ch.epfl.dias.cs422.helpers.builder.skeleton.Scan]]
  * @see [[ch.epfl.dias.cs422.helpers.rel.late.volcano.naive.Operator]]
  */
class LateColumnScan protected (
                                cluster: RelOptCluster,
                                traitSet: RelTraitSet,
                                table: RelOptTable,
                                val tableToStore: ScannableTable => Store,
                                val colName: String
                              ) extends skeleton.Scan[
  ch.epfl.dias.cs422.helpers.rel.late.volcano.naive.Operator
](cluster, traitSet, table)
  with ch.epfl.dias.cs422.helpers.rel.late.volcano.naive.Operator {
  override def toString = "LateColumnScan"

  protected val scannable: LateStandaloneColumnStore = tableToStore(
    table.unwrap(classOf[ScannableTable])
  ).asInstanceOf[LateStandaloneColumnStore]

  /**
    * retrieve store object for the scan's column
    * */
  def getColumn : LateStandaloneColumnStore = {
    scannable
  }

  final override def estimateRowCount(mq: RelMetadataQuery): Double = {
    scannable.getRowCount
  }

  private var current = 0;
  private lazy val col = scannable.getColumn

  override def deriveRowType: RelDataType = {
    val v = table.getRowType.getFieldList.get(scannable.getColumnIndex)
    getCluster.getTypeFactory.createStructType(
      Collections.singletonList(v.getType),
      Collections.singletonList(v.getName)
    )
  }

  override def explainTerms(pw: RelWriter): RelWriter = {
    pw.item("column", colName)
    super.explainTerms(pw)
  }

  /**
    * @inheritdoc
    */
  override def open(): Unit = {
    current = 0
  }

  /**
    * @inheritdoc
    */
  override def next(): Option[LateTuple] = {
    if (current < scannable.getRowCount) {
      val v = col(current)
      val vid = current
      current += 1
      Option(LateTuple(vid, IndexedSeq(v)))
    } else {
      NilLateTuple
    }
  }

  /**
    * @inheritdoc
    */
  override def close(): Unit = {}
}

private[helpers] object LateColumnScan {
  def create(
              cluster: RelOptCluster,
              table: RelOptTable,
              tableToStore: ScannableTable => Store,
              colName: String
            ): LateColumnScan =
    Construct.create(
      classOf[LateColumnScan],
      cluster,
      cluster.traitSet(),
      table,
      tableToStore,
      colName
    )
}