package ch.epfl.dias.cs422.helpers.builder.skeleton

import ch.epfl.dias.cs422.helpers.Construct
import org.apache.calcite.plan.RelTraitSet
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rel.metadata.RelMetadataQuery
import org.apache.calcite.rel.{RelNode, SingleRel}

import java.util

class Drop[TOperator <: RelNode, TInputOperator <: RelNode] protected (
                                                                          input: TInputOperator
                                                                        ) extends logical.LogicalDrop(input) {


  override def copy(
                     traitSet: RelTraitSet,
                     inputs: util.List[RelNode]
                   ): Drop[TOperator, TInputOperator] = {
    assert(inputs.size == 1)
    copy(
      inputs.get(0).asInstanceOf[TInputOperator]
    )
  }

  def copy(
            input: TInputOperator
          ): Drop[TOperator, TInputOperator] = {
    Drop.create(
      input,
      this.getClass
    )
  }
}

private[helpers] object Drop {
  def create[TOperator <: RelNode, TInputOperator <: RelNode](
                                                               input: TInputOperator,
                                                               c: Class[_ <: Drop[TOperator, TInputOperator]]
                                                             ): Drop[TOperator, TInputOperator] = {
    Construct.create(c, input)
  }
}

