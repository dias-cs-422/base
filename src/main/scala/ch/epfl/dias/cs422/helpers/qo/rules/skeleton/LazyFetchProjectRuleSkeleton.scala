package ch.epfl.dias.cs422.helpers.qo.rules.skeleton

import ch.epfl.dias.cs422.helpers.builder.skeleton.logical.LogicalFetch
import org.apache.calcite.plan.{RelOptRuleCall, RelRule}
import org.apache.calcite.rel.RelNode
import org.apache.calcite.rel.logical.LogicalProject

/**
  * RelRule (optimization rule) that finds a reconstruct operator that stitches
  * a new expression (projection over one column) to the late materialized tuple
  * and transforms stitching into a fetch operator with projections.
  *
  * To use this rule: LazyFetchProjectRuleSkeleton.Config.DEFAULT.toRule()
  *
  * @param config configuration parameters of the optimization rule
  */
class LazyFetchProjectRuleSkeleton protected (config: RelRule.Config)
  extends RelRule(
    config
  ) {
  def onMatchHelper(call: RelOptRuleCall): RelNode = ???

  override final def onMatch(call: RelOptRuleCall): Unit = {
    val result = onMatchHelper(call)

    val project : LogicalProject = call.rel(2)

    if (!project.getProjects.isEmpty) {
      assert(result.isInstanceOf[LogicalFetch])
      val fetch = result.asInstanceOf[LogicalFetch]
      assert(fetch.getProjects.nonEmpty)
    }

    call.transformTo(
      result
    )
  }
}