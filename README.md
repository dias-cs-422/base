# CS422 base project

This project contains the main infrastructure required for Project 0 & Project 1.
It provides an integration layer with Calcite and some useful utilities to simplify the interfaces.

### Importing Project in IntelliJ IDEA

1. Checkout the project from version control

2. Enter the clone URL for your repository, click Test to verify the connection and then click clone.

3. Click yes on importing IntelliJ IDEA project file.

4. Verify the project use Java version > 10 and then click ok.

5. If prompted, enable auto-import of sbt project.

6. Build project and happy coding!
